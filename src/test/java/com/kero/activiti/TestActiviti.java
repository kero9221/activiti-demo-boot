package com.kero.activiti;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.zip.ZipInputStream;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.test.ActivitiRule;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestActiviti {

	@Autowired
	private RepositoryService repositoryService;
	
	@Test
	public void deploy() throws FileNotFoundException {
//		String filePath = "E:\\zzl_workspace\\activiti-demo-boot\\src\\main\\resources\\processes\\account\\account.zip";
		String filePath = "E:\\zzl_workspace\\activiti-demo-boot\\src\\main\\resources\\processes\\leave\\leave.zip";
		ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(filePath)); 
		Deployment deployment = repositoryService//
			.createDeployment()//
//			.name("leave-dynamic")//
			.addZipInputStream(zipInputStream)//
			.deploy();
		
		System.out.println("~~~~deploy success, " + ToStringBuilder.reflectionToString(deployment, ToStringStyle.JSON_STYLE));
	}
	
}
