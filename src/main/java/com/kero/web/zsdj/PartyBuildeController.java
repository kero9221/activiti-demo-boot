package com.kero.web.zsdj;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.entity.ApplyInfo;
import com.kero.service.zsdj.PartyBuildeWorkflowService;
import com.kero.vo.WorkflowBean;

import lombok.extern.slf4j.Slf4j;

/**
 * ClassName: PartyBuildeController 
 * @Description: 处理党建流程的控制器
 * @author 郑志良
 * @date 2018年9月28日下午4:13:50
 */
@SuppressWarnings({"rawtypes"})
@Slf4j
@Controller
@RequestMapping("/party_builde")
public class PartyBuildeController {
	
	@Autowired
	private PartyBuildeWorkflowService partyBuildeWorkflowService;
	
	/**
	 * 根据流程定义的key启动最新版本的流程
	 * @param userId 当前登录的用户ID
	 * @param processDefinitionKey 流程定义key
	 * @param applyInfo 请假单实体
	 * @param request
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月31日下午4:55:23
	 */
	@RequestMapping(value="/process-start", method=RequestMethod.POST)
	@ResponseBody
	public R startProcess(
			@RequestParam("userId") String userId,
			@RequestParam("processDefinitionKey") String processDefinitionKey, 
			ApplyInfo applyInfo, 
			HttpServletRequest request) throws Exception{
//		User user = UserUtil.getSessionUser(request.getSession());
		log.info("----applyInfo.userId: {}", userId);
		R r = partyBuildeWorkflowService.startProcess(applyInfo, userId, processDefinitionKey);
		return r;
	}
	
	/**
	 * 我的申请列表
	 * @param startTime 查询条件，查询申请时间大于startTime的记录
	 * @param endTime 查询条件，查询申请时间小于endTime的记录
	 * @param status 请假单状态
	 * @param pageNum
	 * @param pageSize
	 * @param request
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月21日上午11:51:22
	 */
	@RequestMapping(value="/my-apply-list", method=RequestMethod.GET)
	@ResponseBody
	public R<PageResult<ApplyInfo>> myApplyList(
			@RequestParam(value="startTime", required=false) Date startTime,
			@RequestParam(value="endTime", required=false) Date endTime,
			@RequestParam(value="status", required=false) Integer status,
			@RequestParam(value="pageNum", defaultValue="1") Integer pageNum,		
			@RequestParam(value="pageSize", defaultValue="10") Integer pageSize,
			HttpServletRequest request) throws Exception{
//		User user = UserUtil.getSessionUser(request.getSession());
		String userId = request.getParameter("userId");
		R<PageResult<ApplyInfo>> r = partyBuildeWorkflowService.findMyApplyList(userId, startTime, endTime, status, pageNum, pageSize);
		return r;
	}
	
	/**
	 * 任务办理
	 * @param taskId 任务ID
	 * @param approve 是否 同意
	 * @param comment 审批意见
	 * @param request
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月10日上午8:42:29
	 */
	@RequestMapping(value="/complete", method=RequestMethod.POST)
	@ResponseBody
	public R complete(WorkflowBean workflowBean,
			HttpServletRequest request) throws Exception{
		String userId = request.getParameter("userId");
//		User user = UserUtil.getSessionUser(request.getSession());
		R r = partyBuildeWorkflowService.complete(workflowBean, userId);
		return r;
	}
	
}
