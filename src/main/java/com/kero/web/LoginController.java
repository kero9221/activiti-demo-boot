package com.kero.web;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.kero.util.UserUtil;

/**
 * 用户登录的控制器
 * @author 郑志良
 * @date 2018年8月6日上午10:50:32
 */
//@Slf4j
//@Controller
public class LoginController {

	@Autowired
	private IdentityService identityService;
	
	/**
	 * 到登录页面
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月6日上午11:28:49
	 */
	@RequestMapping(value={"/", "/login"}, method=RequestMethod.GET)
	public String loginUI(){
		return "login";
	}
	
	/**
	 * 登录
	 * @param session
	 * @param username
	 * @param password
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月6日上午11:28:42
	 */
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(HttpSession session, RedirectAttributes redirectAttributes ,
			@RequestParam("username") String username,
			@RequestParam("password") String password){
		// 校验用户名与密码是否正确
		boolean checkPassword = identityService.checkPassword(username, password);
		if (!checkPassword){
			// 登录失败，设置提示信息，返回登录页面
			redirectAttributes.addFlashAttribute("msg", "登录失败，用户名或密码不正确");
			return "redirect:/login";
		}
		
		// 查看出该用户
		User user = identityService.createUserQuery().userId(username).singleResult();
		UserUtil.saveUserToSession(session, user);
		
		// 查看出该用户的所有权限组
		List<Group> groupList = identityService.createGroupQuery().groupMember(user.getId()).list();
		if (!CollectionUtils.isEmpty(groupList)){
			// 保存权限组到session中
			UserUtil.saveGroupToSession(session, groupList);
			StringBuilder sb = new StringBuilder();
			for (Group group : groupList) {
				sb.append(group.getName()).append(",");
			}
			sb.deleteCharAt(sb.length() - 1);
			// 保存权限组名到session中
			UserUtil.saveGroupNamesToSession(session, sb.toString());
		}
		
		return "redirect:/main/index";
	}
	
	/**
	 * 退出系统
	 * @param session
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月6日上午11:28:57
	 */
	@RequestMapping(value="/logout")
	public String logout(HttpSession session){
		UserUtil.logout(session);
		return "redirect:/login";
	}
	
}
