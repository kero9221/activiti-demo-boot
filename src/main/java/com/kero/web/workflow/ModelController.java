package com.kero.web.workflow;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kero.common.Const;
import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.service.activiti.WorkflowModelService;
import com.kero.vo.ModelVo;

import lombok.extern.slf4j.Slf4j;

/**
 * 模型相关操作的控制器
 * @author 郑志良
 * @date 2018年8月6日下午2:54:24
 */
@SuppressWarnings("rawtypes")
@Slf4j
@Controller
@RequestMapping("/model")
public class ModelController {

	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private WorkflowModelService workflowModelService;
	
	/**
	 * 模型列表
	 * @param pageNum 分页参数，开始索引
	 * @param pageSize 分页参数，一次取多少条记录
	 * @param modelName 模型名称
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月14日下午3:32:04
	 */
	@RequestMapping(value="/list", method=RequestMethod.GET)
	@ResponseBody
	public R list(
			@RequestParam(value="pageNum", defaultValue="1") Integer pageNum,		
			@RequestParam(value="pageSize", defaultValue="10") Integer pageSize,
			@RequestParam(value="modelName", required=false) String modelName) throws Exception{
		R<PageResult<ModelVo>> r = workflowModelService.list(pageSize, pageNum, modelName);
		return r;
	}
	
	/**
	 * 删除模型
	 * @param modelId 模型ID
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月8日上午11:30:33
	 */
	@RequestMapping(value="/delete/{modelId}", method=RequestMethod.POST)
	@ResponseBody
	public R delete(@PathVariable("modelId") String modelId) throws Exception{
		R r = workflowModelService.delete(modelId);
		return r;
	}
	
	/**
	 * 创建模型
	 * @param modelVo 封装了Model相关的信息
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月27日下午3:44:09
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ResponseBody
	public R create(ModelVo modelVo) throws Exception{
		R<String> r = workflowModelService.create(modelVo); 
		return r;
	}
	
	/**
	 * 部署流程
	 * @param modelId 模型ID
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月6日下午3:24:02
	 */
	@RequestMapping(value="/deploy/{modelId}", method=RequestMethod.POST)
	@ResponseBody
	public R deploy(@PathVariable("modelId") String modelId) throws Exception{
		R r = workflowModelService.deploy(modelId); 
		return r;
	}
	
	/**
	 * 导出模型
	 * @param modelId 模型ID
	 * @param type 以什么类型导出，支持JSON与BPMN两种格式，{@link com.kero.common.Const.ModelExportType}
	 * @param response
	 */
	@RequestMapping(value="/export/{modelId}/{type}")
	public void export(
			@PathVariable("modelId") String modelId, 
			@PathVariable("type") String type, HttpServletResponse response){
		try {
			Model model = repositoryService.getModel(modelId);
			if (model == null){
				response.getWriter().println("model is not exists, modelId: " + modelId);
				response.flushBuffer();
				return;
			}
			
			byte[] modelEditorSource = repositoryService.getModelEditorSource(model.getId());
			JsonNode jsonNode = objectMapper.readTree(modelEditorSource);
			BpmnModel bpmnModel = new BpmnJsonConverter().convertToBpmnModel(jsonNode);
			
			// 处理异常
			if (bpmnModel.getMainProcess() == null){
				response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
				response.getWriter().println("no main process, can't export for type: " + type);
				response.flushBuffer();
				return;
			}
			
			String filename = "";
			byte[] exportBytes = null;
			
			String mainProcessId = bpmnModel.getMainProcess().getId();
			log.info("mainProcessId: " + mainProcessId);
			
			if (Const.ModelExportType.JSON.equals(type)){
				filename = mainProcessId + ".json";
				exportBytes = modelEditorSource;
			} else if (Const.ModelExportType.BPMN.equals(type)){
				filename = mainProcessId + ".bpmn20.xml";
				exportBytes = new BpmnXMLConverter().convertToXML(bpmnModel);
			}
			
			// 设置响应头信息，实现下载功能
			response.setHeader("Content-Disposition", "attachment; filename=" + filename);
			
			ByteArrayInputStream in = new ByteArrayInputStream(exportBytes);
			IOUtils.copy(in, response.getOutputStream());
			
			response.flushBuffer();
		} catch (IOException e) {
			log.error("模型导出失败，modelId={}, type={}", modelId, type, e);
			throw new RuntimeException("模型导出失败", e);
		}
	}
	
}
