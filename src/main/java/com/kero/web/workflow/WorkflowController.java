package com.kero.web.workflow;

import java.io.InputStream;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.service.activiti.WorkflowService;
import com.kero.vo.HistoricProcessInstanceVo;
import com.kero.vo.TaskVo;

/**
 * 工作流相关操作的控制器
 * @author kero
 * @date 2018年8月6日下午10:30:31
 */
@SuppressWarnings({"rawtypes"})
@Controller
@RequestMapping("/workflow")
public class WorkflowController {

	@Autowired
	private WorkflowService workflowService;

	/**
	 * 查询最新版本的流程定义，分页返回
	 * @param pageNum 分页参数，开始索引
	 * @param pageSize 分页参数，取多少条数据
	 * @param name 流程名称
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月14日上午10:43:49
	 */
	@RequestMapping(value="/process-list", method=RequestMethod.GET)
	@ResponseBody
	public R processList(
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize,
			@RequestParam(value="name", required=false) String name) throws Exception {
		R<PageResult<ProcessDefinition>> r = workflowService.processList(pageNum, pageSize, name);
		return r;
	}
	
	/**
	 * 查询我的待办任务
	 * @param startTime 查询参数，查询（任务创建时间）大于该时间的任务
	 * @param endTime 查询参数，查询（任务创建时间）小于该时间的任务
	 * @param pageNum 分页参数，显示第几页的数据
	 * @param pageSize 分页参数，每次显示多少条记录
	 * @param request
	 * @return1
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月20日下午12:15:10
	 */
	@RequestMapping(value="/my-task-list", method=RequestMethod.GET)
	@ResponseBody
	public R<PageResult<TaskVo>> findTodoTaskList(
			@RequestParam(value="startTime", required=false) Date startTime,
			@RequestParam(value="endTime", required=false) Date endTime,
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,		
			@RequestParam(value="pageSize", defaultValue="10") int pageSize,
			HttpServletRequest request) throws Exception{
//		User user = UserUtil.getSessionUser(session);
		String userId = request.getParameter("userId");
		R<PageResult<TaskVo>> r = workflowService.findTodoTaskList(userId, startTime, endTime, pageNum, pageSize);
		return r;
	}
	
	/**
	 * 挂起或激活流程
	 * @param state 状态值，active：激活操作，suspened：挂起操作
	 * @param processInstanceId 流程实例ID
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月7日上午10:48:07
	 */
	@RequestMapping(value="/update-status", method=RequestMethod.GET)
	@ResponseBody
	public R suspendedOrActive(
			@RequestParam("state") String state, 
			@RequestParam("processInstanceId") String processInstanceId) throws Exception{
		R r = workflowService.suspendedOrActive(state, processInstanceId);
		return r;
	}
	
	/**
	 * 获取流程bpmn.xml文件或流程图
	 * @param request
	 * @param response
	 * @param deploymentId 流程部署ID
	 * @param resourceType 流程资源名称
	 * @throws Exception
	 */
	@RequestMapping(value="/view")
	@ResponseBody
	public R view(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("processDefinitionId") String processDefinitionId, 
			@RequestParam("resourceType") String resourceType) throws Exception {
		// 得到指定资源的流
		R<InputStream> r = workflowService.loadResource(processDefinitionId, resourceType);
		
		if (!r.isSuccess()){
			return r;
		}
		
		InputStream inputStream = r.getData();
		// 读取资源到客户端
		ServletOutputStream outputStream = response.getOutputStream();
		byte[] buf = new byte[1024];
		int len = 0;
		while ((len = inputStream.read(buf)) != -1){
			outputStream.write(buf, 0, len);
		}
		
		return R.success();
	}
	
	/**
	 * 获取流程图，高亮当前任务节点
	 * @param processInstanceId 流程实例ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年9月4日上午11:31:08
	 */
	@RequestMapping(value="/get-currentTask-node", method=RequestMethod.GET)
	@ResponseBody
	public R<Map<String, Object>> highlightCurrentTaskNode(@RequestParam("processInstanceId") String processInstanceId) {
		R<Map<String, Object>> r = workflowService.findCurrentTaskCoordinate(processInstanceId);
		return r;
	}
	
	/**
	 * 部署流程定义
	 * @param file 上传的文件
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/deploy", method=RequestMethod.POST)
	@ResponseBody
	public R deploy(@RequestParam("uploadFile") MultipartFile file) throws Exception {
		R r = workflowService.deploy(file);
		return r;
	}
	
	/**
	 * 将流程定义转换成Model
	 * @param processDefinitionId 流程定义ID
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/convert-to-model/{processDefinitionId}", method=RequestMethod.POST)
	@ResponseBody
	public R convertToModel(@PathVariable("processDefinitionId") String processDefinitionId) throws Exception{
		R r = workflowService.convertToModel(processDefinitionId);
		return r;
	}
	
	/**
	 * 删除流程定义
	 * @param deploymentId 流程部署ID
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/process-delete/{deploymentId}", method=RequestMethod.POST)
	@ResponseBody
	public R deleteProcessDefinition(@PathVariable("deploymentId") String deploymentId) throws Exception{
		R r = workflowService.deleteProcess(deploymentId);
		return r;
	}
	
	/**
	 * 删除正在执行的流程实例
	 * @param processInstanceId 流程实例ID
	 * @param deleteReason 删除原因
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月13日上午10:34:06
	 */
	@RequestMapping(value="/running/delete", method=RequestMethod.POST)
	@ResponseBody
	public R deleteRunningProcessInstance(
			@RequestParam("pid") String processInstanceId,
			@RequestParam(value="reason", required=false) String deleteReason) throws Exception{
		R r = workflowService.deleteRunningProcessInstance(processInstanceId, deleteReason);
		return r;
	}
	
	/**
	 * 任务签收
	 * @param taskId
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月9日下午4:51:02
	 */
	@RequestMapping(value="/claim", method=RequestMethod.GET)
	@ResponseBody
	public R claim(@RequestParam("taskId") String taskId, HttpServletRequest request) throws Exception{
//		User user = UserUtil.getSessionUser(request.getSession());
		String userId = request.getParameter("userId");
		R r = workflowService.claim(taskId, userId);
		return r;
	}

	/**
	 * 查询已经结束的流程实例
	 * @param request
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月23日上午11:55:49
	 */
	@RequestMapping(value="/process-finished", method=RequestMethod.GET)
	@ResponseBody
	public R<PageResult<HistoricProcessInstanceVo>> findFinishedProcessInstance(
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,		
			@RequestParam(value="pageSize", defaultValue="10") int pageSize,
			HttpServletRequest request) throws Exception{
		String userId = request.getParameter("userId");
		R<PageResult<HistoricProcessInstanceVo>> r = workflowService.findFinishedProcessInstance(userId, pageNum, pageSize);
		return r;
	}
	
}
