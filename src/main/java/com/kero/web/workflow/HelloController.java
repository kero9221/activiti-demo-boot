package com.kero.web.workflow;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.TransitionImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.image.ProcessDiagramGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kero.service.activiti.WorkflowService;

@Controller
public class HelloController extends WorkflowService {

	/**
	 * @Author 葛明
	 * @Note 读取流程资源
	 * @Date 2017-1-3 15:11
	 * @param processDefinitionId
	 *            流程定义ID
	 * @param resourceName
	 *            资源名称
	 */
	@RequestMapping(value = "/read-resource")
	public void readResource(String processDefinitionId, String resourceName, String pProcessInstanceId,
			HttpServletResponse response) throws Exception {
		// 设置页面不缓存
		response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		ProcessDefinitionQuery pdq = repositoryService.createProcessDefinitionQuery();
		ProcessDefinition pd = pdq.processDefinitionId(processDefinitionId).singleResult();

		if (resourceName.endsWith(".png") && StringUtils.isEmpty(pProcessInstanceId) == false) {
			getActivitiProccessImage(pProcessInstanceId, response);
		} else {
			// 通过接口读取
			InputStream resourceAsStream = repositoryService.getResourceAsStream(pd.getDeploymentId(), resourceName);

			// 输出资源内容到相应对象
			byte[] b = new byte[1024];
			int len = -1;
			while ((len = resourceAsStream.read(b, 0, 1024)) != -1) {
				response.getOutputStream().write(b, 0, len);
			}
		}
	}

	/**
	 * 获取流程图像，已执行节点和流程线高亮显示
	 */
	public void getActivitiProccessImage(String pProcessInstanceId, HttpServletResponse response) {
		try {
			// 获取历史流程实例
			HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(pProcessInstanceId).singleResult();

			if (historicProcessInstance != null) {
				// 获取流程定义
				ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
						.getDeployedProcessDefinition(historicProcessInstance.getProcessDefinitionId());

				// 获取流程历史中已执行节点，并按照节点在流程中执行先后顺序排序
				List<HistoricActivityInstance> historicActivityInstanceList = historyService
						.createHistoricActivityInstanceQuery()
						.processInstanceId(pProcessInstanceId)
						.orderByHistoricActivityInstanceId().asc()
						.list();

				// 已执行的节点ID集合
				List<String> executedActivityIdList = new ArrayList<String>();
				int index = 1;
				// 获取已经执行的节点ID
				for (HistoricActivityInstance activityInstance : historicActivityInstanceList) {
					executedActivityIdList.add(activityInstance.getActivityId());
					index++;
				}

				// 已执行的线集合
				List<String> flowIds = new ArrayList<String>();
				// 获取流程走过的线
				flowIds = getHighLightedFlows(processDefinition, historicActivityInstanceList);

				BpmnModel bpmnModel = repositoryService.getBpmnModel(historicProcessInstance.getProcessDefinitionId());
				// 获取流程图图像字符流
				ProcessDiagramGenerator pec = processEngine.getProcessEngineConfiguration()
						.getProcessDiagramGenerator();
				// 配置字体
				InputStream imageStream = pec.generateDiagram(
						bpmnModel, 
						"png", 
						executedActivityIdList, 
						flowIds, 
						"宋体",
						"微软雅黑", 
						"黑体", 
						null, 
						2.0);

				response.setContentType("image/png");
				OutputStream os = response.getOutputStream();
				int bytesRead = 0;
				byte[] buffer = new byte[8192];
				while ((bytesRead = imageStream.read(buffer, 0, 8192)) != -1) {
					os.write(buffer, 0, bytesRead);
				}
				os.close();
				imageStream.close();
			}
		} catch (Exception e) {
		}
	}

	/**
	 * *获取流程走过的线
	 */
	public List<String> getHighLightedFlows(ProcessDefinitionEntity processDefinitionEntity,
			List<HistoricActivityInstance> historicActivityInstances) {
		// 用以保存高亮的线flowId
		List<String> highFlows = new ArrayList<String>();
		// 对历史流程节点进行遍历
		for (int i = 0; i < historicActivityInstances.size() - 1; i++) {
			// 得到节点定义的详细信息
			ActivityImpl activityImpl = processDefinitionEntity
					.findActivity(historicActivityInstances.get(i).getActivityId());
			// 用以保存后需开始时间相同的节点
			List<ActivityImpl> sameStartTimeNodes = new ArrayList<ActivityImpl>();
			// 将后面第一个节点放在时间相同节点的集合里
			ActivityImpl sameActivityImpl1 = processDefinitionEntity
					.findActivity(historicActivityInstances.get(i + 1).getActivityId());
			sameStartTimeNodes.add(sameActivityImpl1);
			
			for (int j = i + 1; j < historicActivityInstances.size() - 1; j++) {
				// 后续第一个节点
				HistoricActivityInstance activityImpl1 = historicActivityInstances.get(j);
				// 后续第二个节点
				HistoricActivityInstance activityImpl2 = historicActivityInstances.get(j + 1);
				// 如果第一个节点和第二个节点开始时间相同保存
				if (activityImpl1.getStartTime().equals(activityImpl2.getStartTime())) {
					ActivityImpl sameActivityImpl2 = processDefinitionEntity
							.findActivity(activityImpl2.getActivityId());
					sameStartTimeNodes.add(sameActivityImpl2);
				} else {
					// 有不相同跳出循环
					break;
				}
			}
			
			// 取出节点的所有出去的线
			List<PvmTransition> pvmTransitions = activityImpl.getOutgoingTransitions();
			
			// 对所有的线进行遍历
			for (PvmTransition pvmTransition : pvmTransitions) {
				// 如果取出的线的目标节点存在时间相同的节点里，保存该线的id，进行高亮显示
				ActivityImpl pvmActivityImpl = (ActivityImpl) pvmTransition.getDestination();
				if (sameStartTimeNodes.contains(pvmActivityImpl)) {
					highFlows.add(pvmTransition.getId());
				}
			}
		}
		return highFlows;
	}

}
