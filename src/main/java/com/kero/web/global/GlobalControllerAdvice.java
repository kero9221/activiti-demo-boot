package com.kero.web.global;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import com.kero.common.MyDateFormat;

/**
 * 处理所有Controller方法中日期参数为空时SpringMVC出现的日期转换异常
 * @author 郑志良
 * @date 2018年8月21日上午11:14:18
 */
//@Slf4j
@ControllerAdvice(basePackages={"com.kero.web"})
public class GlobalControllerAdvice {

	@Autowired
	private MyDateFormat myDateFormat;
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
//		log.info("-----------initBinder-----------");
		// 允许传入的日期参数为空，不会报错
		binder.registerCustomEditor(Date.class, new CustomDateEditor(myDateFormat, true));
	}
	
}
