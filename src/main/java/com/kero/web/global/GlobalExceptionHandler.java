package com.kero.web.global;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.kero.common.Const.ResponseStatusEnum;
import com.kero.exception.DBException;

/**
 * 全局异常处理器
 * @author 郑志良
 * @date 2018年8月14日下午1:01:21
 */
@Slf4j
@Component
public class GlobalExceptionHandler implements HandlerExceptionResolver {
	
	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		log.error("error", ex);
		
		ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
		mv.addObject("code", ResponseStatusEnum.ERROR.getCode());
//		mv.addObject("msg", ex.getMessage());
		
		String msg = "服务异常，详情请查看服务器日志";
		if (ex instanceof DBException) {
			msg = ((DBException)ex).getMessage();
		} else if (ex instanceof GlobalException) {
			msg = ((GlobalException)ex).getMessage();
		}
		
		mv.addObject("msg", msg);
		
		return mv;
	}

}
