package com.kero.web.global;

/**
 * ClassName: GlobalException 
 * @Description: 自定义全局异常
 * @author 郑志良
 * @date 2018年9月28日上午11:38:47
 */
public class GlobalException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public GlobalException(String message) {
		super(message);
	}
	
	public GlobalException(String message, Throwable e) {
		super(message, e);
	}
	
}
