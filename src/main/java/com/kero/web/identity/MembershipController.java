package com.kero.web.identity;

import org.activiti.engine.IdentityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kero.common.R;

import lombok.extern.slf4j.Slf4j;

/**
 * 维护用户与用户组关联关系的控制器
 * @author 郑志良
 * @date 2018年8月31日上午11:15:50
 */
@SuppressWarnings({"rawtypes"})
@Slf4j
@Controller
@RequestMapping("/workflow/membership")
public class MembershipController {

	@Autowired
	private IdentityService identityService;
	
	/**
	 * 建立用户与组的关联关系
	 * @param userId 用户ID
	 * @param groupId 用户组ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月31日上午11:07:56
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ResponseBody
	public R create(
			@RequestParam("userId") String userId, 
			@RequestParam("groupId") String groupId) {
		if (StringUtils.isBlank(userId) || StringUtils.isBlank(groupId)) {
			return R.error("参数错误，userId和groupId都不能为空");
		}
		
		long count = identityService.createUserQuery().userId(userId).count();
		if (count < 1) {
			return R.error("关联的用户不存在，userId: " + userId);
		}
		
		count = identityService.createGroupQuery().groupId(groupId).count();
		if (count < 1) {
			return R.error("关联的用户组不存在，groupId: " + groupId);
		}
		
		try {
			identityService.createMembership(userId, groupId);
			return R.success();
		} catch (Exception e) {
			log.error("用户与用户组关联失败，该关联关系已经存在，userId: {}, groupId: {}", userId, groupId, e);
			return R.error("用户与用户组关联失败，该关联关系已经存在，userId: " + userId + ", groupId: " + groupId);
		}
	}
	
	/**
	 * 解除用户与用户组的关联关系
	 * @param userId 用户ID
	 * @param groupId 用户组ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月31日上午11:11:33
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ResponseBody
	public R delete(
			@RequestParam("userId") String userId, 
			@RequestParam("groupId") String groupId) {
		if (StringUtils.isBlank(userId) || StringUtils.isBlank(groupId)) {
			return R.error("参数错误，userId和groupId都不能为空");
		}
		
		try {
			identityService.deleteMembership(userId, groupId);
			return R.success();
		} catch (Exception e) {
			log.error("解除用户组与用户关联失败，userId: {}, groupId: {}", userId, groupId);
			return R.error("解除用户组与用户关联失败，userId: " + userId + ", groupId: " + groupId);
		}
	}
	
}
