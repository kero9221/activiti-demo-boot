package com.kero.web.identity;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kero.common.PageResult;
import com.kero.common.R;

/**
 * 用户组相关的操作的控制器
 * @author 郑志良
 * @date 2018年8月23日下午2:23:47
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Slf4j
@Controller
@RequestMapping("/workflow/group")
public class GroupController {

	@Autowired
	private IdentityService identityService;
	
	/**
	 * 用户组列表
	 * @param pageNum
	 * @param pageSize
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月23日下午2:27:20
	 */
	@RequestMapping(value="/list", method=RequestMethod.GET)
	@ResponseBody
	public R<Group> groupList(
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize){
		PageResult<Group> page = new PageResult<Group>(pageNum, pageSize);
		GroupQuery query = identityService.createGroupQuery();
		List<Group> groupList = query.orderByGroupId().asc().listPage(page.getFirstIndex(), page.getPageSize());
		page.setRows(groupList);
		page.setTotal(query.count());
		return R.success().data(page);
	}
	
	
	/**
	 * 新增用户组
	 * @param groupEntity 用户组实体
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月23日下午2:34:57
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ResponseBody
	public R create(GroupEntity groupEntity){
		String groupId = groupEntity.getId();
		String groupName = groupEntity.getName();
		if (StringUtils.isBlank(groupId) 
				|| StringUtils.isBlank(groupName)){
			return R.error("参数错误，group的Id和name都不能为空");
		}
		
		long count = identityService.createGroupQuery().groupId(groupId).count();
		if (count > 1) {
			return R.error("新增用户组失败，该用户组已经存在，groupId: " + groupId);
		}
		
		groupEntity.setType("assignment");
		identityService.saveGroup(groupEntity);
		log.debug("---用户组新增成功，组ID:{}, name: {}, type: {}", groupEntity.getId(), groupEntity.getName(), groupEntity.getType());
		return R.success("新增用户组成功，groupId: " + groupEntity.getId()); 
	}	
	
	/**
	 * 删除用户组
	 * @param groupId
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月23日下午2:35:51
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ResponseBody
	public R delete(@RequestParam("id") String groupId){
		if (StringUtils.isBlank(groupId)){
			return R.error("参数错误，groupId不能为空");
		}
		
		long count = identityService.createGroupQuery().groupId(groupId).count();
		if (count < 1) {
			return R.error("用户组删除失败，不存在的用户组，groupId: " + groupId);
		}
		
		identityService.deleteGroup(groupId);
		log.debug("删除组成功，组ID: {}", groupId);
		return R.success("删除用户组成功， groupId: " + groupId);
	}
	
}
