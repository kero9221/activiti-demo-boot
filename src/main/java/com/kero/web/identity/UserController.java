package com.kero.web.identity;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kero.common.PageResult;
import com.kero.common.R;

/**
 * 用户相关的操作的控制器
 * 
 * @author 郑志良
 * @date 2018年8月6日上午11:52:45
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Slf4j
@Controller
@RequestMapping("/workflow/user")
public class UserController {

	@Autowired
	private IdentityService identityService;
	
	/**
	 * 用户列表
	 * @param pageNum
	 * @param pageSize
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月23日下午2:22:55
	 */
	@RequestMapping(value="/list", method=RequestMethod.GET)
	@ResponseBody
	public R<PageResult<User>> userList(
			@RequestParam(value="pageNum", defaultValue="1") int pageNum,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize){
		PageResult<User> page = new PageResult<User>(pageNum, pageSize);
		UserQuery query = identityService.createUserQuery();
		List<User> userList = query//
								.orderByUserId().asc()//
								.listPage(page.getFirstIndex(), page.getPageSize());
		page.setRows(userList);
		page.setTotal(query.count());
		return R.success().data(page);
	}
	
	/**
	 * 新增用户
	 * @param user
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月23日下午2:22:44
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ResponseBody
	public R userCreate(UserEntity user){
		if (user == null || StringUtils.isBlank(user.getId())){
			return R.error("用户新增失败，参数错误，userId不能为空");
		}
		
		// 校验用户是否已经存在
		long count = identityService.createUserQuery().userId(user.getId()).count();
		if (count > 1) {
			return R.error("用户新增失败，该用户已经存在：userId: " + user.getId());
		} 
		
		// 添加用户
		identityService.saveUser(user);
		// 建立用户与用户组的关系
		//identityService.createMembership(user.getId(), groupId);
		log.debug("--用户新增成功，用户ID: {}", user.getId());
		return R.success("用户新增成功，新增用户ID: " + user.getId());
	}
	
	/**
	 * 删除用户
	 * @param userId
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月23日下午2:22:29
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ResponseBody
	public R userDelete(@RequestParam("id") String userId){
		if (StringUtils.isBlank(userId)){
			return R.error("参数错误， userId不为为空");
		}
		
		long count = identityService.createUserQuery().userId(userId).count();
		if (count < 1) {
			return R.error("用户删除失败，不存在的用户，userId: " + userId);
		}
		
		identityService.deleteUser(userId);
		log.debug("--用户删除成功，用户ID: {}", userId);
		return R.success("用户删除成功， userId: " + userId);
	}
	
}
