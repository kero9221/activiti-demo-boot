package com.kero.config;

import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.stereotype.Component;

@Component
public class MyProcessEngineConfigurationConfigurer implements ProcessEngineConfigurationConfigurer {

	@Override
	public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
		System.out.println("------------MyProcessEngineConfigurationConfigurer----------------");
		processEngineConfiguration
			.setActivityFontName("宋体")
			.setAnnotationFontName("宋体")
			.setLabelFontName("宋体");
	}

}
