package com.kero.config;

import java.util.Arrays;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.kero.filter.HeadersCORSFilter;

@Configuration
public class CustomWebMvcConfig implements WebMvcConfigurer {

//	@Bean
//	public PageHelper pageHelper() {
//		return new PageHelper();
//	}
	
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
	
	@Bean
	public FilterRegistrationBean<Filter> myFilter(){
		FilterRegistrationBean<Filter> filterRegistryBean = new FilterRegistrationBean<>();
		filterRegistryBean.setFilter(new HeadersCORSFilter());
		filterRegistryBean.setUrlPatterns(Arrays.asList("/*"));
		return filterRegistryBean;
	}
	
	
}
