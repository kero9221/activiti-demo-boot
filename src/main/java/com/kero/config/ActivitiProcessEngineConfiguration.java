package com.kero.config;

import org.activiti.spring.boot.AbstractProcessEngineAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Activiti相关的配置类
 * @author 郑志良
 * @date 2018年9月11日上午9:51:46
 */
@Configuration
public class ActivitiProcessEngineConfiguration extends AbstractProcessEngineAutoConfiguration {

	// 配置相关信息，如数据源，事务管理器，流程引擎相关配置
	
//	@Bean
//	public ObjectMapper objectMapper() {
//		System.out.println("-----初始化ObjectMapper-----");
//		return new ObjectMapper();
//	}
	
}
