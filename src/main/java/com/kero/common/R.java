package com.kero.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;
import com.kero.common.Const.ResponseStatusEnum;

/**
 * 用来统一返回给前端的响应对象
 * 
 * @author 郑志良
 * @date 2018年7月18日下午5:04:18
 */
@SuppressWarnings("rawtypes")
@JsonSerialize(include=Inclusion.NON_NULL)
public class R<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final R RECORD_IS_EMPTY = R.success("没有找到相关的记录~");
	public static final R RECORD_NOT_EXISTS = R.error("操作的记录不存在~");
	
	public static final R NEED_LOGIN = R.error(ResponseStatusEnum.NEED_LOGIN.getCode(), ResponseStatusEnum.NEED_LOGIN.getDesc());
	public static final R PARAM_ERROR = R.error(ResponseStatusEnum.PARAM_ERROR.getCode(), ResponseStatusEnum.PARAM_ERROR.getDesc());
//	public static final R SUCCESS = R.error(ResponseStatusEnum.SUCCESS.getCode(), ResponseStatusEnum.SUCCESS.getDesc());
//	public static final R ERROR = R.error(ResponseStatusEnum.ERROR.getCode(), ResponseStatusEnum.ERROR.getDesc());

	public static final R DB_INSERT_ERROR = R.error(500201, "数据库异常，数据插入失败~");
	public static final R DB_UPDATE_ERROR = R.error(500202, "数据库异常，数据更新失败~");
	public static final R DB_DELETE_ERROR = R.error(500203, "数据库异常，数据删除失败~");
	public static final R DB_SELECT_ERROR = R.error(500204, "数据库异常，数据查询失败~");
	
	
	/** 响应的状态码 */
	private int code;
	/** 响应的信息 */
	private String msg;
	/** 响应的数据 */
	private T data;
	
	private R(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public static R success(int code, String msg){
		return new R(code, msg);
	}
	
	public static R success(String msg){
		return success(ResponseStatusEnum.SUCCESS.getCode(), msg);
	}
	
	public static R success(){
		return success(ResponseStatusEnum.SUCCESS.getCode(), ResponseStatusEnum.SUCCESS.getDesc());
	}
	
	public R data(T data){
		this.data = data;
		return this;
	}

	public static R error(int code, String msg){
		return new R(code, msg);
	}
	
	public static R error(String msg){
		return error(ResponseStatusEnum.ERROR.getCode(), msg);
	}
	
	public static R error(){
		return error(ResponseStatusEnum.ERROR.getCode(), ResponseStatusEnum.ERROR.getDesc());
	}

	// 请求是否成功的标识
	@JsonIgnore
	public boolean isSuccess(){
		return this.code == ResponseStatusEnum.SUCCESS.getCode();
	}
	
	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public T getData() {
		return data;
	}
	
}
