package com.kero.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

import lombok.extern.slf4j.Slf4j;

/**
 * 日期转换器
 * @author 郑志良
 * @date 2018年8月9日下午3:40:06
 */
@Slf4j
public class MyDateConverter implements Converter<String, Date> {
	
	/** 默认日期转换格式，带时分秒 */
	private static final String DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	/** 默认日期转换格式，不带时分秒 */
	private static final String DEFAULT_PATTERN_DATE = "yyyy-MM-dd";
	private static final SimpleDateFormat DEFAULT_SDF = new SimpleDateFormat(DEFAULT_PATTERN);
	private static final SimpleDateFormat DEFAULT_SDF_DATE = new SimpleDateFormat(DEFAULT_PATTERN_DATE);
	
	@Override
	public Date convert(String source) {
		try {
			log.debug("----------解析日期字符串：{}", source);
			return DEFAULT_SDF.parse(source);
		} catch (ParseException e) {
			try {
				return DEFAULT_SDF_DATE.parse(source);
			} catch (ParseException e1) {
				log.info("---日期解析出错, 解析的字符串为:{}", source);
				throw new RuntimeException("---日期解析出错", e1);
			}
		}
	}

}
