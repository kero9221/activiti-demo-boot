package com.kero.common;

/**
 * 常量类
 * @author 郑志良
 * @date 2018年7月18日下午5:11:04
 */
public class Const {
	/** 默认的日期格式 */
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 模型导出类型
	 * @author 郑志良
	 * @date 2018年9月4日下午4:22:15
	 */
	public interface ModelExportType {
		/** 以JSON格式导出 */
		String JSON = "JSON";
		/** 以BPMN格式导出 */
		String BPMN = "BPMN";
	}
	
	/**
	 * 流程实例状态类型
	 * @author 郑志良
	 * @date 2018年8月28日上午9:05:39
	 */
	public interface ProcessInstanceStatus{
		/** 激活状态 */
		String ACTIVE = "active";
		/** 挂起状态 */
		String SUSPEND = "suspend";
	}
	
	/**
	 * 流程定义资源类型
	 * @author 郑志良
	 * @date 2018年8月15日下午2:36:40
	 */
	public interface ProcessDefinitionResourceType{
		/** xml文件 */
		String XML = "xml";
		/** 图片资源 */
		String IMAGE = "image";
	}
	
	/**
	 * 请假单状态
	 * @author 郑志良
	 * @date 2018年7月25日上午11:11:26
	 */
	public enum ProcessStatusEnum{
		/** 初始录入 */
		INITAL_INPUT(0, "初始录入"),
		/** 审批中 */
		RUNNING(1, "审批中"),
		/** 已通过 */
		PASSED(2, "已通过"),
		/** 已取消 */
		CANCELED(3, "已取消"),
		/** 已删除 */
		DELETED(4, "已删除");
		
		private int code;
		private String desc;
		
		private ProcessStatusEnum(int code, String desc) {
			this.code = code;
			this.desc = desc;
		}
		public int getCode() {
			return code;
		}
		public String getDesc() {
			return desc;
		}
	}
	
	/**
	 * 我申请的任务的状态枚举类
	 * @author 郑志良
	 * @date 2018年7月23日上午10:28:36
	 */
	public enum MyTaskStatusEnum{
		/** 审核中 */
		AUDITING(0, "审核中"),
		/** 已结束 */
		FINISHED(1, "已结束");
		
		private int code;
		private String desc;
		
		private MyTaskStatusEnum(int code, String desc) {
			this.code = code;
			this.desc = desc;
		}
		public int getCode() {
			return code;
		}
		public String getDesc() {
			return desc;
		}
	
	}
	
	/**
	 * 定义请求响应的各种状态的枚举类
	 * @author 郑志良
	 * @date 2018年7月18日下午5:13:10
	 */
	public enum ResponseStatusEnum{
		/** 操作成功 */
		SUCCESS(0, "操作成功"),
		/** 操作失败 */
		ERROR(1, "操作失败"),
		/** 需要强制登录 */
		NEED_LOGIN(2, "需要强制登录"),
		/** 没有权限的操作 */
		NO_PRIVILEGE(3, "没有权限的操作"),
		/** 参数错误 */
		PARAM_ERROR(4, "参数错误");
		
		private int code;
		private String desc;
		
		private ResponseStatusEnum(int code, String desc) {
			this.code = code;
			this.desc = desc;
		}
		public int getCode() {
			return code;
		}
		public String getDesc() {
			return desc;
		}
	}
	
}
