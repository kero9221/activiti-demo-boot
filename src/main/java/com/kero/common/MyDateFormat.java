package com.kero.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

/**
 * 处理将日期字符串转换为日期对象
 * @author 郑志良
 * @date 2018年8月21日下午2:35:29
 */
@Component
public class MyDateFormat extends SimpleDateFormat {
	private static final long serialVersionUID = 1L;

	private static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private static final String DATETIME_PATTERN_NO_SECOND = "yyyy-MM-dd HH:mm";

    private static final String DATE_PATTERN = "yyyy-MM-dd";

    private static final String MONTH_PATTERN = "yyyy-MM";
	
	
	@Override
	public Date parse(String source) throws ParseException {
		try {
			source = StringUtils.trim(source);
			return super.parse(source);
		} catch (Exception e) {
			try {
				return DateUtils.parseDate(
						source, 
						new String[]{
								DATETIME_PATTERN, 
								DATETIME_PATTERN_NO_SECOND, 
								DATE_PATTERN, 
								MONTH_PATTERN});
			} catch (Exception e1) {
				throw new RuntimeException("----日期转换异常，不支持的日期格式：" + source, e1);
			}
		}
	}
	
}
