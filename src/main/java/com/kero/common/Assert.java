package com.kero.common;

import com.kero.web.global.GlobalException;

/**
 * 断言类
 * @author 郑志良
 * @date 2018年9月21日下午2:37:15
 */
public class Assert {

	public static void notNull(Object obj, String message) {
		if (obj == null) {
			throw new GlobalException(message);
		}
	}
	
}
