package com.kero.common;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 用来封装响应信息
 * 
 * @author 郑志良
 * @date 2018年7月23日上午10:35:16
 */
@SuppressWarnings("unused")
public class PageResult<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/** 默认显示第一页的数据 */
	public static final int DEFAULT_PAGE_NUM = 1;
	/** 默认每页显示的记录数 */
	public static final int DEFAULT_PAGE_SIZE = 10;
	
	/** 查询第几页的数据 */
	private int pageNum;
	/** 每页显示的记录数 */
	private int pageSize;
	/** 分页参数,开始索引 */
	private int firstIndex;
	/** 总页数 */
	private int totalPage;
	
	/** 总记录数 */
	private long total;
	/** 返回的数据的集合，默认是一个空的集合 */
	@SuppressWarnings("unchecked")
	private List<T> rows = Collections.EMPTY_LIST;

	public PageResult(int pageNum, int pageSize) {
		this.pageNum = (pageNum < DEFAULT_PAGE_NUM) ? DEFAULT_PAGE_NUM : pageNum;
		this.pageSize = (pageSize < 1) ? DEFAULT_PAGE_SIZE : pageSize;
		this.firstIndex = (pageNum - 1) * this.pageSize;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getPageNum() {
		return pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	@JsonIgnore
	public int getFirstIndex() {
		return firstIndex;
	}
	
	/** 获取总页数 */
	public int getTotalPage() {
		return ((int)this.total - 1) / pageSize + 1;
	}

}
