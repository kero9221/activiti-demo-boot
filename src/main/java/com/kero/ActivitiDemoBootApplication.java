package com.kero;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan("com.kero.mapper")
@EnableTransactionManagement
@SpringBootApplication(exclude= {
		org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class, 
		org.activiti.spring.boot.SecurityAutoConfiguration.class
		})
public class ActivitiDemoBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivitiDemoBootApplication.class, args);
	}
}
