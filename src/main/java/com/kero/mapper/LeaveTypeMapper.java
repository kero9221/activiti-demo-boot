package com.kero.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kero.entity.LeaveType;

public interface LeaveTypeMapper extends BaseMapper<LeaveType> {
    
}