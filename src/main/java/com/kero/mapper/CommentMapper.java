package com.kero.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kero.entity.Comment;

public interface CommentMapper extends BaseMapper<Comment> {
    
}