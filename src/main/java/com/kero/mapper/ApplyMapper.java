package com.kero.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kero.entity.Apply;

public interface ApplyMapper extends BaseMapper<Apply> {
    
}