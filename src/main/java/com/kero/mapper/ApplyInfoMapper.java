package com.kero.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kero.entity.ApplyInfo;

public interface ApplyInfoMapper extends BaseMapper<ApplyInfo> {
    
}