package com.kero.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain=true)
public class Apply {
	
    private Long id;

    private Date applyTime;

    private String candidateUsers;

    private Date endTime;

    private String formData;

    private String formId;

    private String processInstanceId;

    private String reason;

    private String roleId;

    private Date startTime;

    private Integer status;

    private String title;

    private String userId;

    private Long leaveType;

    private Date createTime;

    private Date updateTime;

}