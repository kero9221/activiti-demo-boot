package com.kero.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain=true)
public class Comment {
    private Long id;

    private Boolean approve;

    private Date auditTime;

    private Long leaveId;

    private String message;

    private String processInstanceId;

    private String taskId;

    private String userId;
    
    private Date createTime;
    
    private Date updateTime;

}