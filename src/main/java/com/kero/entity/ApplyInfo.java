package com.kero.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain=true)
public class ApplyInfo {
	
    private Long id;

    private String userId;

    private String candidateUser;

    private String formData;

    private String name;

    private Integer sex;

    private String nation;

    private String from;

    private String vocation;

    private String education;

    private String degree;

    private String workAddress;

    private String homeAddress;

    private Date birthday;

    private String processInstanceId;

    private Integer status;

    private Date createTime;

    private Date updateTime;

}