package com.kero.filter;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * 使用CORS解决跨域问题
 * @author 郑志良
 * @date 2018年8月28日上午11:05:53
 */
@Slf4j
public class HeadersCORSFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request,
			ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		log.info("--------------{}---------------", req.getRequestURI());

		Map<String, String[]> map = req.getParameterMap();
		Set<Entry<String, String[]>> entrySet = map.entrySet();
		for (Entry<String, String[]> entry : entrySet) {
			log.info("key: {}, value: {}", entry.getKey(), entry.getValue());
		}

		HttpServletResponse resp = (HttpServletResponse) servletResponse;
		String method = req.getMethod().toUpperCase();
		log.info("----------method: {}", method);
		if ("OPTIONS".equals(method) || "POST".equals(method)){
			resp.setHeader("Access-Control-Allow-Origin", "*");
			resp.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
			resp.setHeader("Access-Control-Max-Age", "3600");
			resp.setHeader("Access-Control-Allow-Headers", "Accept,Content-Type,x-requested-with,Authorization");
			// response.setHeader("Access-Control-Allow-Credentials","true");
		} else if ("GET".equals(method)){
			resp.setHeader("Access-Control-Allow-Origin", "*");
			resp.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		}
		chain.doFilter(request, servletResponse);
	}

	@Override
	public void destroy() {
	}

}