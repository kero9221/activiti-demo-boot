//package com.kero.service.identity;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.codehaus.jackson.type.TypeReference;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//import com.kero.common.entity.Role;
//import com.kero.common.entity.User;
//import com.kero.util.HttpUtil;
//
//import swtech.common.entity.ReturnMsg;
//
///**
// * 使用httpclient发送http请求权限模块服务接口
// * @author 郑志良
// * @date 2018年8月31日上午11:13:25
// */
////@Service
//public class HttpIdentityService {
//	
//	/** 使用的协议 */
//	@Value("${http.protocol}")
//	private String protocol;
//	/** 请求的主机IP */
//	@Value("${http.host}")
//	private String host;
//	/** 请求的端口号 */
//	@Value("${http.port}")
//	private String port;
//	
//	/** 请求的资源名称 */
//	@Value("${getUserById}")
//	private String resourceName_getUserById;
//	@Value("${selectRoleUserInfo}")
//	private String resourceName_selectRoleUserInfo;
//	@Value("${selectRoleInfo}")
//	private String resourceName_selectRoleInfo;
//	
//	/**
//	 * 读取配置文件，拼装请求的uri，如："http://192.168.0.213:20896/htUserService/getUserById"
//	 * @return
//	 *
//	 * @author 郑志良
//	 * @date 2018年8月31日上午9:32:53
//	 */
//	private String getResourceName(String resouceName) {
//		return protocol + "://" + host + ":" + port + resouceName;
//	}
//	
//	/**
//	 * 根据用户ID查询用户信息
//	 * @param id 用户ID
//	 * @return
//	 * @throws Exception
//	 *
//	 * @author 郑志良
//	 * @date 2018年8月31日上午9:44:05
//	 */
//	public ReturnMsg<Map<String,User>> getUserById(String id) throws Exception {
//		Map<String, String> paramMap = new HashMap<>();
//		paramMap.put("id", id);
//		ReturnMsg<Map<String, User>> result = HttpUtil.get(getResourceName(resourceName_getUserById), paramMap, new TypeReference<ReturnMsg<Map<String, User>>>() {});
//		return result;
//	}
//	
//	public ReturnMsg<Map<String,List<Role>>> selectRoleUserInfo() throws Exception {
//		ReturnMsg<Map<String, List<Role>>> result = HttpUtil.get(getResourceName(resourceName_selectRoleUserInfo), null, new TypeReference<ReturnMsg<Map<String, List<Role>>>>() {});
//		return result;
//	}
//	
//	public ReturnMsg<Map<String,List<Role>>> selectRoleInfo() throws Exception {
//		ReturnMsg<Map<String, List<Role>>> result = HttpUtil.get(getResourceName(resourceName_selectRoleInfo), null, new TypeReference<ReturnMsg<Map<String, List<Role>>>>() {});
//		return result;
//	}
//	
//}
