package com.kero.service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.activiti.engine.IdentityService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.identity.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.kero.common.R;
import com.kero.entity.Apply;
import com.kero.service.apply.LeaveWorkflowService;

/**
 * 设置邮件相关信息
 * @author kero
 * @date 2018年8月12日上午8:26:49
 */
@Slf4j
@Component
@Transactional
public class SendMailInfo implements ExecutionListener {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private IdentityService identityService;
	@Autowired
	private LeaveWorkflowService leaveWorkflowService;
	
	@Override
	public void notify(DelegateExecution execution) throws Exception {
		log.info("-------------------SendMailInfo----------------------");
		
		// 获取申请人ID，查询出申请人信息
		String userId = (String) execution.getVariable("applyUserId");
		User user = identityService.createUserQuery().userId(userId).singleResult();
		
		// 查询出请假单信息
		String businessKey = execution.getProcessBusinessKey();
		R<Apply> r = leaveWorkflowService.findLeaveById(new Long(businessKey));
		Apply leave = r.getData();
		
		// 设置变量属性
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("to", user.getEmail());
		variables.put("from", "kero921@163.com");
		variables.put("name", StringUtils.defaultString(user.getFirstName()) + StringUtils.defaultString(user.getLastName()));
		
		// 设置请假相关信息变量
		variables.put("startTime", leave.getStartTime());
		variables.put("endTime", leave.getEndTime());
		variables.put("reason", leave.getReason());
		
		// 设置超时提醒时间为：结束时间+1天
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(leave.getEndTime());
		calendar.add(Calendar.MILLISECOND, 1000 * 30); // 超时提醒30秒
		variables.put("reportBackTimeout", calendar.getTime());
		
		log.info("----variables: {}", variables);
		
		// 设置本地流程变量
		execution.setVariablesLocal(variables);
	}
	
}
