package com.kero.service.apply;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * 监听销假任务的监听器
 * @author 郑志良
 * @date 2018年8月10日下午4:15:31
 */
@Slf4j
@Component
@Transactional
public class ReportBackEndProcessor implements TaskListener {
	private static final long serialVersionUID = 1L;

//	@Autowired
//	private RuntimeService runtimeService;
//	@Autowired
//	private LeaveService leaveService;
	
	@Override
	public void notify(DelegateTask delegateTask) {
		log.info("-------------ReportBackEndProcessor-------------");
//		String processInstanceId = delegateTask.getProcessInstanceId();
//		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
//		String businessKey = processInstance.getBusinessKey();
//		
//		Leave leave = leaveRepository.findOne(new Long(businessKey));
//		leave.setRealityStartTime((Date)delegateTask.getVariable("realityStartTime"));
//		leave.setRealityEndTime((Date)delegateTask.getVariable("realityEndTime"));
//		
//		leaveRepository.save(leave);
	}

}
