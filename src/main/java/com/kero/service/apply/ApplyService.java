package com.kero.service.apply;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kero.common.Assert;
import com.kero.common.Const.ProcessStatusEnum;
import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.entity.Apply;
import com.kero.exception.DBException;
import com.kero.mapper.ApplyMapper;

/**
 * 处理请假单实体相关操作的service
 * @author 郑志良
 * @date 2018年8月14日上午11:54:00
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Service
public class ApplyService {

	@Autowired
	private ApplyMapper applyMapper;
	
	/**
	 * 查询当前用户所有申请信息
	 * @param userId 当前用户ID
	 * @param pageNum 分页参数，开始索引
	 * @param pageSize 分页参数，一次取多少条数据
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:29:54
	 */
	@Transactional(readOnly=true)
	public R<PageResult<Apply>> findByCondition(String userId, Date startTime, Date endTime, Integer status, int pageNum, int pageSize) throws Exception{
		Assert.notNull(userId, "参数错误，userId为能为空！");
		
		// 创建分页对象
		PageResult<Apply> page = new PageResult<Apply>(pageNum, pageSize);
		
		QueryWrapper<Apply> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("user_id", userId);
		if (status != null) {
			queryWrapper.eq("status", status);
		}
		if (startTime != null) {
			queryWrapper.ge("apply_time", startTime);
		}
		if (endTime != null) {
			queryWrapper.le("apply_time", endTime);
		}
		queryWrapper.orderByDesc("apply_time");
		
		try {
			IPage<Apply> _page = applyMapper.selectPage(new Page<Apply>(page.getPageNum(), page.getPageSize()), queryWrapper);
			page.setRows(_page.getRecords());
			page.setTotal(_page.getTotal());
			
			return R.success().data(page);
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败~", e);
		}
	}
	
	/**
	 * 根据请假单ID和当前用户ID查询请假单信息
	 * @param id 请假单ID
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:32:10
	 */
	@Transactional(readOnly=true)
	public R<Apply> findByIdAndUserId(Long id, String userId) {
		// 校验入参
		Assert.notNull(id, "参数错误，id为能为空！");
		Assert.notNull(userId, "参数错误，userId为能为空！");
		
		try {
			Apply apply = applyMapper.selectOne(
					new QueryWrapper<Apply>()
						.eq("id", id)
						.eq("user_id", userId)
					);
			
			if (apply == null) {
				return R.RECORD_IS_EMPTY;
			}
			return R.success().data(apply);
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败~", e);
		}
	}
	
	/**
	 * 更新
	 * @param apply 请假单实体
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:33:02
	 */
	@Transactional
	public R<?> update(Apply apply, String userId) {
		Assert.notNull(apply, "参数错误，apply不能为空");
		Assert.notNull(apply.getId(), "参数错误，apply.id不能为空");
		
		
		try {
			R<?> r = countByIdAndUserId(apply.getId(), userId);
			if (!r.isSuccess()) {
				return r;
			}
			
			apply.setUpdateTime(new Date());
			int count = applyMapper.updateById(apply);
			if (count > 0) {
				return R.success();
			}
			return R.error();
		} catch (Exception e) {
			throw new DBException("数据库异常，更新失败", e);
		}
		
	}
	
	/**
	 * @Description: 根据id和用户id统计记录数 
	 * @param id
	 * @param userId
	 * @return
	 * @throws Exception   
	 * @author 郑志良
	 * @date 2018年10月8日下午5:36:43
	 */
	public R<?> countByIdAndUserId(Long id, String userId) throws Exception {
		try {
			Integer selectCount = applyMapper.selectCount(
					new QueryWrapper<Apply>()
						.eq("id", id)
						.eq("user_id", userId)
					);
			
			if (selectCount < 0) {
				return R.RECORD_NOT_EXISTS;
			}
			
			return R.success();
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败", e);
		}
	}
	
	/**
	 * 保存请假单信息
	 * @param apply 请假单实体
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:34:45
	 */
	@Transactional
	public R<Apply> save(Apply apply, String userId) {
		Assert.notNull(apply, "参数错误，apply不能为空");
		
		try {
			// 设置属性
			apply.setUserId(userId);
			// 设置状态为"审批中"
			apply.setStatus(ProcessStatusEnum.RUNNING.getCode());
			apply.setApplyTime(new Date());
			apply.setCreateTime(new Date());
			apply.setUpdateTime(new Date());
			// 保存apply实体
			int count = applyMapper.insert(apply);
			if (count > 0) {
				return R.success().data(apply);
			}
			return R.error();
		} catch (Exception e) {
			throw new DBException("数据库异常，保存出错~", e);
		}
	}
	
	/**
	 * 删除
	 * @param id 
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:35:06
	 */
	@Transactional
	public R delete(Long id, String userId) throws Exception {
		// 校验入参
		Assert.notNull(id, "参数错误，id为能为空！");
		Assert.notNull(userId, "参数错误，userId为能为空！");
		
		// 查询要删除的数据，如果不存在，则返回错误信息
		R<?> r = countByIdAndUserId(id, userId);
		if (!r.isSuccess()) {
			return r;
		}
		
		try {
			// 再执行删除操作
			int count = applyMapper.deleteById(id);
			if (count > 0) {
				return R.success();
			}
			return R.error();
		} catch (Exception e) {
			throw new DBException("数据库异常，删除失败", e);
		}
	}

	
	/**
	 * 根据Id查询
	 * @param id
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月15日下午3:38:28
	 */
	public R<Apply> findById(Long id) {
		Assert.notNull(id, "参数错误，id为能为空！");
		
		try {
			Apply apply = applyMapper.selectById(id);
			if (apply == null){
				return R.error("没有找到请假单对象, id: " + id);
			}
			return R.success().data(apply);
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败", e);
		}
	}
	
}
