package com.kero.service.apply;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.kero.web.global.GlobalException;

import lombok.extern.slf4j.Slf4j;

/**
 * 动态设置任务办理人
 * @author 郑志良
 * @date 2018年8月31日下午2:26:05
 */
@Slf4j
@Component
public class ManageTaskListener implements TaskListener {
	private static final long serialVersionUID = 1L;

	@Override
	public void notify(DelegateTask delegateTask) {
		log.info("-------------ManageTaskListener--------------");
		// 从变量中获取任务办理的候选人
		String candidateUsers = (String) delegateTask.getVariable("candidateUsers");
		log.info("----任务办理候选人，candidateUsers: {}", candidateUsers);
		
		String assignee = delegateTask.getAssignee();
		log.info("----任务办理人：assignee: {}", assignee);
		
		// 当任务办理候选人与任务办理人都为空时，则抛出异常
		if (StringUtils.isBlank(candidateUsers) && StringUtils.isBlank(assignee)) {
			log.error("任务办理失败，任务办理候选人为空，taskId: {}, taskName: {}" + delegateTask.getId(), delegateTask.getName());
			throw new GlobalException("任务办理失败，任务办理候选人为空，taskId: " + delegateTask.getId() + ", taskName: " + delegateTask.getName());
		}
		
		List<String> candidateUserList = new ArrayList<String>();
		// 多个任务办理候选人使用","号分隔
		candidateUsers = StringUtils.trim(candidateUsers);
		String[] userArr = candidateUsers.split(",");
		for (String user : userArr) {
			candidateUserList.add(user);
		}
		
		// 设置下一个办理任务候选人
		delegateTask.addCandidateUsers(candidateUserList);
	}

}
