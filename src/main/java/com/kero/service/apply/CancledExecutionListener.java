package com.kero.service.apply;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.kero.common.Const;
import com.kero.common.R;
import com.kero.entity.Apply;
import com.kero.web.global.GlobalException;

import lombok.extern.slf4j.Slf4j;

/**
 * 监听请假流程取消事件的监听器
 * @author 郑志良
 * @date 2018年9月3日下午5:22:49
 */
@Slf4j
@Transactional
@Component
public class CancledExecutionListener implements ExecutionListener {
	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplyService applyService;
	
	@Override
	public void notify(DelegateExecution execution) throws Exception {
		log.info("-----------CancledExecutionListener-------------");
		
		// 获取业务key
		String businessKey = execution.getProcessBusinessKey();
		if (StringUtils.isBlank(businessKey)) {
			throw new GlobalException("businessKey不能为空");
		}
		// 根据业务key找到对应的请假单信息
		R<Apply> r = applyService.findById(new Long(businessKey));
		if (!r.isSuccess()) {
			throw new GlobalException(r.getMsg());
		}
		
		// 设置申请单状态为"已取消"状态
		Apply apply = r.getData();
		apply.setStatus(Const.ProcessStatusEnum.CANCELED.getCode());
		
		// 更新状态到数据库
		R<?> _r = applyService.update(apply, apply.getUserId());
		if (!_r.isSuccess()) {
			throw new GlobalException(r.getMsg());
		}
	}

}
