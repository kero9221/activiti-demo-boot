package com.kero.service.apply;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.kero.common.R;
import com.kero.entity.Apply;
import com.kero.web.global.GlobalException;

import lombok.extern.slf4j.Slf4j;

/**
 * 监听重新申请任务完成事件
 * @author 郑志良
 * @date 2018年8月10日下午3:57:48
 */
@Slf4j
@Component
@Transactional
public class AfterReapplyListener implements TaskListener {
	private static final long serialVersionUID = 1L;

	@Autowired
	private RuntimeService runtimeService; 
	@Autowired
	private ApplyService applyService;
	
	
	@Override
	public void notify(DelegateTask delegateTask) {
		log.info("-------------AfterModifyApplyContentProcessor-------------");
		
		// 获取业务key
		String processInstanceId = delegateTask.getProcessInstanceId();
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
		String businessKey = processInstance.getBusinessKey();
		
		// 根据businessKey找到对应的申请记录
		R<Apply> r = applyService.findById(new Long(businessKey));
		if (!r.isSuccess()){
			throw new GlobalException(r.getMsg());
		}
		
		// 更新获取formData数据到数据库
		Apply apply = r.getData();
		Object formData = delegateTask.getVariable("formData");
		if (formData != null){
			apply.setFormData((String)formData);
		}
		
//		leave.setStartTime((Date)delegateTask.getVariable("startTime"));
//		leave.setEndTime((Date)delegateTask.getVariable("endTime"));
//		leave.setReason((String)delegateTask.getVariable("reason"));
		
		// 如果更新了请假类型，则更新请假类型（该操作只在请假流程才会被调用）
		Object leaveTypeIdStr = delegateTask.getVariable("leaveTypeId");
		if (leaveTypeIdStr != null){
			Apply updateApply = new Apply();
			updateApply.setId(apply.getId());
			updateApply.setLeaveType(new Long((String)leaveTypeIdStr));
			R<?> _r = applyService.update(updateApply, apply.getUserId());
			
			if (!r.isSuccess()) {
				throw new GlobalException(_r.getMsg());
			}
		}
		
	}

}
