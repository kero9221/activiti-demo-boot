/**
 * 
 */
package com.kero.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.TaskFormData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kero.common.R;

import lombok.extern.slf4j.Slf4j;

/**
 * 动态表单流程相关操作的业务层
 * @author 郑志良
 * @date 2018年8月7日下午12:19:41
 */
@Slf4j
@Service
public class DynamicFormService {

	@Autowired
	private FormService formService;
	@Autowired
	private TaskService taskService;
	
	/**
	 * 获取任务表单数据
	 * @param taskId 任务ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月7日下午1:05:22
	 */
	public R<Map<String, Object>> getTaskFormData(String taskId){
		if (StringUtils.isBlank(taskId)){
			return R.PARAM_ERROR;
		}
		
		long count = taskService.createTaskQuery()//
			.taskId(taskId)//
			.count();
		if (count < 1){
			return R.error("要办理的任务不存在，任务ID: " + taskId);
		}
		Map<String, Object> properties = new HashMap<String, Object>();
		TaskFormData taskFormData = formService.getTaskFormData(taskId);
		List<FormProperty> formProperties = taskFormData.getFormProperties();
		for (FormProperty formProperty : formProperties) {
			log.info("-----type: {}", formProperty.getType().getName());
			Map<String, String> values = (Map<String, String>) formProperty.getType().getInformation("values");
			if (values != null){
				for (Map.Entry<String, String> entry : values.entrySet()){
					log.info("enum, key: {}, value: {}", entry.getKey(), entry.getValue());
				}
				properties.put(formProperty.getId(), values);
			}
		}
		properties.put("taskFormData", taskFormData);
		
		return R.success().data(properties);
	}	
	
	/**
	 * 获取表单可写的属性ID
	 * @param taskId
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月7日下午3:07:52
	 */
	public List<String> getWriteableTaskFormProperty(String taskId){
		List<String> writeablePropertyIds = new ArrayList<String>();
		
		TaskFormData taskFormData = formService.getTaskFormData(taskId);
		List<FormProperty> formProperties = taskFormData.getFormProperties();
		for (FormProperty formProperty : formProperties) {
			if (formProperty.isWritable()){
				writeablePropertyIds.add(formProperty.getId());
			}
		}
		return writeablePropertyIds;
	}

	/**
	 * 办理任务
	 * @param taskId
	 * @param properties
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月7日下午3:12:33
	 */
	public R complete(String taskId, Map<String, String> properties){
		formService.submitTaskFormData(taskId, properties);
		return R.success("任务办理成功");
	}
	
}
