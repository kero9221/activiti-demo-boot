package com.kero.service.zsdj;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kero.common.Assert;
import com.kero.common.Const.ProcessStatusEnum;
import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.entity.ApplyInfo;
import com.kero.exception.DBException;
import com.kero.mapper.ApplyInfoMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 处理请假单实体相关操作的service
 * @author 郑志良
 * @date 2018年8月14日上午11:54:00
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Slf4j
@Service
public class ApplyInfoService {

	@Autowired
	private ApplyInfoMapper applyInfoMapper;
	
	/**
	 * 查询当前用户所有请假单信息
	 * @param userId 当前用户ID
	 * @param pageNum 分页参数，开始索引
	 * @param pageSize 分页参数，一次取多少条数据
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:29:54
	 */
	@Transactional(readOnly=true)
	public R<PageResult<ApplyInfo>> findByCondition(String userId, Date startTime, Date endTime, Integer status, int pageNum, int pageSize) throws Exception{
		Assert.notNull(userId, "参数错误，userId为能为空！");
		
		log.info("userId:{}, startTime:{}, endTime:{}, status:{}, pageNum:{}, pageSize:{}", userId, startTime, endTime, status, pageNum, pageSize);
		
		// 创建分页对象
		PageResult<ApplyInfo> page = new PageResult<ApplyInfo>(pageNum, pageSize);
		
//		PageHelper.startPage(page.getPageNum(), page.getPageSize());
//		ApplyInfoExample example = new ApplyInfoExample();
//		Criteria criteria = example.createCriteria();
		
		QueryWrapper<ApplyInfo> queryWrapper = new QueryWrapper<ApplyInfo>();
//		criteria.andUserIdEqualTo(userId);
		queryWrapper.eq("user_id", userId);
		if (status != null) {
//			criteria.andStatusEqualTo(status);
			queryWrapper.eq("status", status);
		}
		if (startTime != null) {
//			criteria.andCreateTimeGreaterThanOrEqualTo(startTime);
			queryWrapper.ge("create_time", startTime);
		}
		if (endTime != null) {
//			criteria.andCreateTimeLessThanOrEqualTo(endTime);
			queryWrapper.le("create_time", endTime);
		}
		
		try {
//			List<ApplyInfo> list = applyInfoMapper.selectByExample(example);
			IPage<ApplyInfo> _page = applyInfoMapper.selectPage(new Page<ApplyInfo>(page.getPageNum(), page.getPageSize()),  queryWrapper);
//			PageInfo<ApplyInfo> pageInfo = new PageInfo<>(list);
			page.setRows(_page.getRecords());
			page.setTotal(_page.getTotal());
			
			log.info("record size:{}", page.getTotal());
			
			return R.success().data(page);
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败~", e);
		}
	}
	
	/**
	 * 根据请假单ID和当前用户ID查询请假单信息
	 * @param id 请假单ID
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:32:10
	 */
	@Transactional(readOnly=true)
	public R<ApplyInfo> findByIdAndUserId(Long id, String userId) throws Exception {
		// 校验入参
		Assert.notNull(id, "参数错误，id为能为空！");
		Assert.notNull(userId, "参数错误，userId为能为空！");
		
		log.info("id:{}, userId:{}", id, userId);
		
		try {
//			ApplyInfoExample example = new ApplyInfoExample();
//			example.createCriteria()
//				.andIdEqualTo(id)
//				.andUserIdEqualTo(userId);
			ApplyInfo applyInfo = applyInfoMapper.selectOne(
				new QueryWrapper<ApplyInfo>()
				.eq("id", id)
				.eq("user_id", userId));
			
			if (applyInfo == null) {
				return R.RECORD_IS_EMPTY;
			}
			
			return R.success().data(applyInfo);
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败~", e);
		}
	}
	
	public R<?> countByIdAndUserId(Long id, String userId) throws Exception {
		try {
			Integer selectCount = applyInfoMapper.selectCount(
					new QueryWrapper<ApplyInfo>()
						.eq("id", id)
						.eq("user_id", userId)
					);
			
			if (selectCount < 0) {
				return R.RECORD_NOT_EXISTS;
			}
			
			return R.success();
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败", e);
		}
	}
	
	/**
	 * 更新请假单信息
	 * @param applyInfo 请假单实体
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:33:02
	 */
	@Transactional
	public R<?> update(ApplyInfo applyInfo, String userId) throws Exception {
		Assert.notNull(applyInfo, "参数错误，applyInfo不能为空");
		
		R<?> r = countByIdAndUserId(applyInfo.getId(), userId);
		if (!r.isSuccess()) {
			return r;
		}
		
		try {
			applyInfo.setUpdateTime(new Date());
//			int count = applyInfoMapper.updateByPrimaryKeySelective(applyInfo);
			int count = applyInfoMapper.updateById(applyInfo);
			if (count > 0) {
				return R.success();
			}
			return R.error();
		} catch (Exception e) {
			throw new DBException("数据库异常，更新失败", e);
		}
		
	}
	
	/**
	 * 保存请假单信息
	 * @param applyInfo 请假单实体
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:34:45
	 */
	@Transactional
	public R<ApplyInfo> save(ApplyInfo applyInfo, String userId) {
		Assert.notNull(applyInfo, "参数错误，applyInfo不能为空");
		
		log.info("applyInfo:{}", ToStringBuilder.reflectionToString(applyInfo));
		
		try {
			// 设置属性
			applyInfo.setUserId(userId);
			// 设置状态为"审批中"
			applyInfo.setStatus(ProcessStatusEnum.RUNNING.getCode());
			applyInfo.setCreateTime(new Date());
			applyInfo.setUpdateTime(new Date());
			// 保存applyInfo实体
			int count = applyInfoMapper.insert(applyInfo);
			if (count > 0) {
				return R.success().data(applyInfo);
			}
			return R.error();
		} catch (Exception e) {
			throw new DBException("数据库异常，保存出错~", e);
		}
	}
	
	/**
	 * 删除
	 * @param id 
	 * @param userId 当前用户ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午9:35:06
	 */
	@Transactional
	public R delete(Long id, String userId) throws Exception {
		log.info("id:{}, userId:{}", id, userId);
		// 校验入参
		Assert.notNull(id, "参数错误，id为能为空！");
		Assert.notNull(userId, "参数错误，userId为能为空！");
		
		// 查询要删除的数据，如果不存在，则返回错误信息
		R<?> r = countByIdAndUserId(id, userId);
		if (!r.isSuccess()) {
			return r;
		}
		
		try {
			// 再执行删除操作
			int count = applyInfoMapper.deleteById(id);
			if (count > 0) {
				return R.success();
			}
			return R.error();
		} catch (Exception e) {
			throw new DBException("数据库异常，删除失败", e);
		}
	}

	/**
	 * 根据Id查询
	 * @param id
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月15日下午3:38:28
	 */
	public R<ApplyInfo> findById(Long id) {
		Assert.notNull(id, "参数错误，id为能为空！");
		
		try {
			ApplyInfo applyInfo = applyInfoMapper.selectById(id);
			if (applyInfo == null){
				return R.error("没有找到请假单对象, id: " + id);
			}
			return R.success().data(applyInfo);
		} catch (Exception e) {
			throw new DBException("数据库异常，查询失败", e);
		}
	}
	
}
