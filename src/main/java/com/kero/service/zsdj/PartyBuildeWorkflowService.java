package com.kero.service.zsdj;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.editor.language.json.converter.util.CollectionUtils;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.entity.ApplyInfo;
import com.kero.entity.Comment;
import com.kero.mapper.CommentMapper;
import com.kero.service.activiti.WorkflowService;
import com.kero.vo.ApplyInfoVo;
import com.kero.vo.HistoricProcessInstanceVo;
import com.kero.vo.WorkflowBean;

import lombok.extern.slf4j.Slf4j;

/**
 * 处理请假单流程相关的Service
 * @author 郑志良
 * @date 2018年8月15日上午10:43:59
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Slf4j
@Service
public class PartyBuildeWorkflowService extends WorkflowService {

	@Autowired
	private CommentMapper commentMapper;
	@Autowired
	private ApplyInfoService applyInfoService;
	
	/**
	 * 启动流程实例
	 * @param applyInfo 请假实体
	 * @param userId 用户ID
	 * @param processDefinitionKey 流程定义Key
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月27日下午5:23:16
	 */
	@Transactional
	public R startProcess(
			ApplyInfo applyInfo, 
			String userId, 
			String processDefinitionKey) throws Exception {
		if (StringUtils.isBlank(userId)) {
			return R.NEED_LOGIN;
		}
		
		if (StringUtils.isBlank(processDefinitionKey)){
			return R.error("参数错误，processDefinitionKey不能为空");
		}
		
		// 这里不做判断，因为当当前任务是最后一个任务时，就无需传递candidateUsers参数
//		String candidateUsers = StringUtils.trim(applyInfo.getCandidateUsers());
//		if (StringUtils.isBlank(candidateUsers)) {
//			String message = "流程启动失败，任务办理候选人不能为空，candidateGroup：" + candidateUsers;
//			log.error(message);
//			return R.error(message);
//		}
		
		// 保存请假单信息
		R<ApplyInfo> r = applyInfoService.save(applyInfo, userId);
		if (!r.isSuccess()){
			return r;
		}
		
		try {
			// 定义businessKey，用来关联流程与业务
			String businessKey = applyInfo.getId().toString();
			// 设置申请人ID
			identityService.setAuthenticatedUserId(userId);
			// 设置流程变量(设置下一任务办理候选人)，启动流程
			Map<String, Object> variables = new HashMap<>();
			variables.put("candidateUsers", StringUtils.trim(applyInfo.getCandidateUser()));
			ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey, businessKey, variables);
			// 设置流程实例ID
			applyInfo.setProcessInstanceId(processInstance.getId());
			log.info("start process of {key={}, businessKey={}, pid={}, variables={}}",
					new Object[]{processDefinitionKey, businessKey, processInstance.getId(), variables});
			
			return R.success("流程启动成功，流程实例ID: " + processInstance.getId());
		} catch (Exception e) {
			log.error("流程启动失败，processDefinitionKey: {}", processDefinitionKey);
			throw new RuntimeException("流程启动失败，processDefinitionKey: " + processDefinitionKey);
		} 
	}
	
	/**
	 * 查询我的申请列表
	 * @param userId
	 * @return
	 *
	 * @author 郑志良
	 * @param endTime 
	 * @param startTime 
	 * @throws Exception 
	 * @date 2018年8月9日上午11:39:22
	 */
	public R<PageResult<ApplyInfo>> findMyApplyList(String userId, Date startTime, Date endTime, Integer status, int pageNum, int pageSize) throws Exception{
		R<PageResult<ApplyInfo>> r = applyInfoService.findByCondition(userId, startTime, endTime, status, pageNum, pageSize);
		if (!r.isSuccess()){
			return r;
		}
		
		List<ApplyInfo> applyInfoList = r.getData().getRows();
		
		if (CollectionUtils.isEmpty(applyInfoList)) {
			return r;
		}
		
		
		for (ApplyInfo applyInfo : applyInfoList) {
			ApplyInfoVo vo = new ApplyInfoVo();
			vo.setId(applyInfo.getId());
			vo.setUserId(applyInfo.getUserId());
			vo.setBirthday(applyInfo.getBirthday());
			vo.setCandidateUser(applyInfo.getCandidateUser());
			vo.setCreateTime(applyInfo.getCreateTime());
			vo.setUpdateTime(applyInfo.getUpdateTime());
			vo.setDegree(applyInfo.getDegree());
			vo.setEducation(applyInfo.getEducation());
			vo.setFormData(applyInfo.getFormData());
			vo.setFrom(applyInfo.getFrom());
			vo.setName(applyInfo.getName());
			vo.setNation(applyInfo.getNation());
			
			String processInstanceId = applyInfo.getProcessInstanceId();
			if (processInstanceId != null){
				HistoricProcessInstance hpi = historyService
												.createHistoricProcessInstanceQuery()//
												.processInstanceId(processInstanceId)//
												.singleResult();
				HistoricProcessInstanceVo hpiVo = packageVoService.packageHistoricProcessInstanceVo(hpi);
				vo.setHistoricProcessInstanceVo(hpiVo);
			}
		}
		return r;
	}

//	/**
//	 * 查询所有流程定义，带分页返回
//	 * @param name 流程定义名称
//	 * @param pageSize 分页参数，开始索引
//	 * @param pageNum 分页参数，每次取多少条数据
//	 * @return
//	 * @throws Exception
//	 *
//	 * @author 郑志良
//	 * @date 2018年8月16日上午11:20:46
//	 */
//	public R<Page<ProcessDefinition>> processList(String name, int pageSize, int pageNum) throws Exception {
//		return workflowService.processList(pageSize, pageNum, name);
//	}

	/**
	 * 根据ID查询
	 * @param taskId 任务ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月9日下午4:48:47
	 */
	public R<ApplyInfo> getApplyInfoInfo(String taskId) throws Exception {
		if (StringUtils.isBlank(taskId)){
			return R.error("参数错误，任务ID不能为空");
		}
		
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		if (task == null){
			return R.error("要办理的任务不存在");
		}
		
		String processInstanceId = task.getProcessInstanceId();
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).active().singleResult();
		if (processInstance == null){
			return R.error("任务所属的流程实例不存在或不可用");
		}
		
		String businessKey = processInstance.getBusinessKey();
		R<ApplyInfo> r = applyInfoService.findById(Long.parseLong(businessKey));
		return r;
	}

//	/**
//	 * 任务签收
//	 * @param taskId
//	 * @return
//	 *
//	 * @author 郑志良
//	 * @date 2018年8月9日下午4:53:17
//	 */
//	public R claim(String taskId, String userId) throws Exception {
//		return workflowService.claim(taskId, userId);
//	}

	/**
	 * 任务办理
	 * @param applyInfo 封装了请假单信息的实体
	 * @param workflowBean 封装了表单提交的相关信息
	 * @param variables 流程变量
	 * @return
	 *
	 * @author 郑志良
	 * @param userId 
	 * @throws Exception 
	 * @date 2018年8月10日上午9:17:16
	 */
	@Transactional
	public R complete(WorkflowBean workflowBean, String userId) throws Exception {
		// 校验用户是否登录
		if (StringUtils.isBlank(userId)){
			return R.NEED_LOGIN;
		}
		
		// 校验任务办理人是否为空
//		String candidateUsers = workflowBean.getCandidateUsers();
//		if (StringUtils.isBlank(candidateUsers)) {
//			return R.error("任务办理失败，任务的办理人不能为空，candidateUsers: " + candidateUsers);
//		}
		
		// 校验请假单ID
		Long applyInfoId = workflowBean.getId();
		if (applyInfoId == null){
			return R.error("参数错误，applyInfoId不能为空");
		}
		
		// 校验任务Id
		String taskId = workflowBean.getTaskId();
		if (StringUtils.isBlank(taskId)){
			return R.error("任务办理失败，参数taskId不能为空");
		}
		
		// 查询任务
		Task task = taskService.createTaskQuery()//
						.taskId(taskId)//
						.taskAssignee(userId)//
						.active()//
						.singleResult();
		if (task == null){
			return R.error("没有找到对应的任务，taskId: " + taskId);
		}
		
		// 添加审批相关信息
		String message = StringUtils.defaultString(workflowBean.getComment());;
		boolean approve = workflowBean.isApprove();
		Comment comment = new Comment();
		comment.setApprove(approve);
		comment.setAuditTime(new Date());
//		comment.setApplyInfoId(applyInfoId);
		comment.setMessage(message);
		comment.setTaskId(taskId);
		comment.setUserId(userId);
		comment.setProcessInstanceId(task.getProcessInstanceId());
		comment.setCreateTime(new Date());
		comment.setUpdateTime(new Date());
		// 保存审批相关的信息
		commentMapper.insert(comment);
		
		// 完成任务，设置流程变量
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("candidateUsers", StringUtils.trim(workflowBean.getCandidateUsers()));
		variables.put("approve", approve);
		variables.put("formData", workflowBean.getFormData());
		log.info("----------variables[{}]", variables.toString());
		taskService.complete(taskId, variables);
		
		return R.success("任务办理成功，任务ID: " + taskId);
	}

	/**
	 * 根据ID查询ApplyInfo
	 * @param id
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月11日上午11:14:54
	 */
	public R<ApplyInfo> findApplyInfoById(Long id){
		R r = applyInfoService.findById(id);
		return r;
	}	
	
}
