/**
 * 
 */
package com.kero.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kero.common.R;
import com.kero.service.activiti.WorkflowBaseService;

/**
 * 外置表单业务层
 * @author 郑志良
 * @date 2018年8月8日上午10:16:23
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Slf4j
@Service
@Transactional
public class FormkeyService extends WorkflowBaseService {

	/**
	 * 查询外置表单流程列表
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月8日上午10:25:25
	 */
	public R<List<ProcessDefinition>> processDefinitionList(){
		List<ProcessDefinition> processDefinitionList = repositoryService.createProcessDefinitionQuery()//
			.processDefinitionKey("leave-formkey")//
			.active()//
			.list();
		
		return R.success().data(processDefinitionList);
	}

	/**
	 * 获取启动时的外置表单
	 * @param processDefinitionId
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月8日上午10:57:38
	 */
	public R getStartForm(String processDefinitionId) {
		if (StringUtils.isBlank(processDefinitionId)){
			return R.PARAM_ERROR;
		}
		
		long count = repositoryService.createProcessDefinitionQuery()//
			.processDefinitionId(processDefinitionId)//
			.count();
		if (count != 1){
			return R.error("没有找到对应的流程定义，processDefinitionId: " + processDefinitionId);
		}
		
		Object startForm = formService.getRenderedStartForm(processDefinitionId);
		if (startForm == null){
			return R.error("该流程定义没有设置startForm属性，processDefinitionId： " + processDefinitionId);
		}
		return R.success().data(startForm);
	}
	
	/**
	 * 启动流程实例
	 * @param parameterMap 封装了请求参数的Map
	 * @param processDefinitionId 流程定义的ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月8日下午12:22:53
	 */
	@Transactional
	public R startProcess(Map<String, String[]> parameterMap, String processDefinitionId, String userId){
		if (StringUtils.isBlank(processDefinitionId)){
			return R.PARAM_ERROR;
		}
		
		// 查询流程定义对象
		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()//
			.processDefinitionId(processDefinitionId)//
			.singleResult();
		if (processDefinition == null){
			return R.error("没有找到对应的流程定义，processDefinitionId: " + processDefinitionId);
		}
		
		// 获取表单数据
		Map<String, Object> formProperties = getProperties(parameterMap);
		
		try{
			// 设置发起人
			identityService.setAuthenticatedUserId(userId);
			// 启动流程实例
			String processDefinitionKey = processDefinition.getKey();
			ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey, formProperties);
			if (processInstance == null){
				return R.error("流程启动失败");
			}
			return R.success("流程启动成功，启动的流程实例ID: " + processInstance.getId());
		} finally {
			identityService.setAuthenticatedUserId(null);
		}
	}

	/** 获取请求参数*/
	public Map<String, Object> getProperties(Map<String, String[]> parameterMap) {
		// 获取请求参数
		Map<String, Object> formProperties = new HashMap<String, Object>();
		for (Map.Entry<String, String[]> entry : parameterMap.entrySet()){
			String key = entry.getKey();
			if (key.startsWith("fp_")){
				formProperties.put(key.split("_")[1], entry.getValue()[0]);
			}
		}
		return formProperties;
	}
	
	
	/**
	 * 根据部署ID删除流程定义，级联删除该流程实例
	 * @param deploymentId 流程部署ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月8日上午11:39:17
	 */
	@Transactional
	public R deleteProcessByDeploymentId(String deploymentId){
		if (StringUtils.isBlank(deploymentId)){
			return R.PARAM_ERROR;
		}
		
		try {
			repositoryService.deleteDeployment(deploymentId, true);
			return R.success("流程定义删除成功");
		} catch (Exception e) {
			log.error("流程定义删除失败", e);
			return R.error("流程定义删除失败");
		}
	}
	
	/**
	 * 获取任务表单
	 * @param taskId 任务ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月8日下午12:23:43
	 */
	public R getTaskForm(String taskId){
		if (StringUtils.isBlank(taskId)){
			return R.PARAM_ERROR;
		}
		
		long count = taskService.createTaskQuery().taskId(taskId).count();
		if (count != 1){
			return R.error("没有找到对应的任务对象，taskId: " + taskId);
		}
		
		Object startForm = formService.getRenderedTaskForm(taskId);
		if (startForm == null){
			return R.error("当前任务节点没有设置任务表单，taskId： " + taskId);
		}
		return R.success().data(startForm);		
	}
	
	/**
	 * 任务办理
	 * @param map 封装
	 * @param taskId 任务办理
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月8日下午2:43:18
	 */
	@Transactional
	public R completeTask(Map<String, String[]> parameterMap, String taskId){
		if (StringUtils.isBlank(taskId)){
			return R.PARAM_ERROR;
		}
		
		// 查询任务对象
		long count = taskService.createTaskQuery().taskId(taskId).count();
		if (count != 1){
			return R.error("没有找到要办理的任务，taskId: " + taskId);
		}
		
		// 获取表单数据
		Map<String, Object> formProperties = getProperties(parameterMap);
		
		// 启动流程实例
		taskService.complete(taskId, formProperties);
		log.info("任务办理成功，任务ID: {}", taskId);
		return R.success("任务办理成功，任务ID: " + taskId);
	}
	
	
}
