package com.kero.service.activiti;

import java.util.Collections;
import java.util.List;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.impl.persistence.entity.SuspensionState;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.kero.common.Const;
import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.vo.HistoricProcessInstanceVo;
import com.kero.vo.ProcessInstanceVo;

import lombok.extern.slf4j.Slf4j;

/**
 * 处理工作流与流程实例相关的service
 * @author 郑志良
 * @date 2018年8月15日下午12:13:43
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Slf4j
@Service
public class WorkflowProcessInstanceService extends WorkflowBaseService {

	/**
	 * 根据流程实例状态查询流程实例，”激活“或”挂起“
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @param status 流程实例 的状态
	 * @date 2018年8月7日上午9:28:33
	 */
	public R<List<ProcessInstanceVo>> findProcessInstanceByStatus(int status) throws Exception{
		ProcessInstanceQuery query = runtimeService.createProcessInstanceQuery();
		
		if (status == SuspensionState.ACTIVE.getStateCode()){
			query.active();
		} else if (status == SuspensionState.SUSPENDED.getStateCode()) {
			query.suspended();
		}
		
		List<ProcessInstance> processInstanceList = query.orderByProcessInstanceId().desc().list();
		// 如果结果集为空，则返回 一个空集合
		if (CollectionUtils.isEmpty(processInstanceList)){
			processInstanceList = Collections.EMPTY_LIST;
		}
		
		// 组装ProcessInstanceVo对象
		List<ProcessInstanceVo> processInstanceVoList = packageVoService.packageProcessInstanceVoList(processInstanceList);
		
		return R.success().data(processInstanceVoList);
	}
	
	/**
	 * 挂起或激活流程实例
	 * @param state 状态，该状态包括suspend(挂起)和active(激活)
	 * @param processInstanceId 流程实例ID
	 *
	 * @author 郑志良
	 * @date 2018年8月7日上午10:34:58
	 */
	public R suspendedOrActive(String state, String processInstanceId) throws Exception {
		if (StringUtils.isBlank(state) || StringUtils.isBlank(processInstanceId)){
			return R.error("参数错误，state和processInstanceId都不能为空");	
		}
		
		if (Const.ProcessInstanceStatus.ACTIVE.equals(state)){
			runtimeService.activateProcessInstanceById(processInstanceId);
			return R.success("流程激活成功！");
		} else if (Const.ProcessInstanceStatus.SUSPEND.equals(state)){
			runtimeService.suspendProcessInstanceById(processInstanceId);
			return R.success("流程挂起成功！");
		}
		
		return R.error("参数不合法，state参数必须是'active'或'suspend'");
	}
	
	/**
	 * 删除正在运行的流程实例
	 * @param processInstanceId 流程实例ID
	 * @param deleteReason 删除原因
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月13日上午10:26:47
	 */
	public R deleteRunningProcessInstance(String processInstanceId, String deleteReason) throws Exception {
		if (StringUtils.isBlank(processInstanceId)){
			log.error("参数错误，processInstanceId不能为空");
			return R.error("参数错误，processInstanceId不能为空");
		}
		
		try {
			runtimeService.deleteProcessInstance(processInstanceId, deleteReason);
			return R.success("删除流程实例成功, processInstanceId: " + processInstanceId);
		} catch (Exception e) {
			log.error("删除流程实例失败，processInstanceId: {}, deleteReason: {}", processInstanceId, deleteReason, e);
			return R.error("删除流程实例失败, processInstanceId: " + processInstanceId);
		}
	}
	
	/**
	 * 删除历史流程实例
	 * @param processInstanceId 流程实例ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月13日上午10:28:32
	 */
	public R deleteFinishedProcessInstance(String processInstanceId) throws Exception {
		if (StringUtils.isBlank(processInstanceId)){
			log.error("参数错误，processInstanceId不能为空");
			return R.error("参数错误，processInstanceId不能为空");
		}
		
		try {
			historyService.deleteHistoricProcessInstance(processInstanceId);
			return R.success("删除流程实例成功");
		} catch (Exception e) {
			log.error("删除流程实例失败，processInstanceId: {}, deleteReason: {}", processInstanceId, e);
			return R.error("删除流程实例失败");
		}
	}
	
	/**
	 * 查询已经完成的流程实例
	 * @param userId 用户ID
	 * @return 
	 *
	 * @author 郑志良
	 * @param pageSize 
	 * @param pageNum 
	 * @date 2018年8月7日下午3:33:20
	 */
	public R<PageResult<HistoricProcessInstanceVo>> findFinishedProcessInstance(String userId, int pageNum, int pageSize) throws Exception {
		PageResult<HistoricProcessInstanceVo> page = new PageResult(pageNum, pageSize);
		
		HistoricProcessInstanceQuery query = historyService.createHistoricProcessInstanceQuery();
		
		// 如果userId不为空，则只查询userId发起的流程列表
		if(StringUtils.isNotBlank(userId)){
			query.startedBy(userId);
		}
		
		List<HistoricProcessInstance> historicProcessList = query//
																.finished()//
																.orderByProcessInstanceEndTime().desc()//
																.listPage(page.getFirstIndex(), page.getPageSize());
						
		List<HistoricProcessInstanceVo> voList =  packageVoService.packageHistoricProcessInstanceVoList(historicProcessList);
		page.setRows(voList);
		page.setTotal(query.count());
		
		return R.success().data(page);
	}
	
}
