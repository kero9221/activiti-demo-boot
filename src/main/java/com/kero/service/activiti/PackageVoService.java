package com.kero.service.activiti;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kero.common.R;
import com.kero.entity.Apply;
import com.kero.service.apply.ApplyService;
import com.kero.vo.HistoricProcessInstanceVo;
import com.kero.vo.ModelVo;
import com.kero.vo.ProcessDefinitionVo;
import com.kero.vo.ProcessInstanceVo;
import com.kero.vo.TaskVo;

/**
 * 用来组装Vo对象的servcie
 * @author 郑志良
 * @date 2018年8月28日上午9:38:00
 */
@SuppressWarnings({"unchecked"})
@Service
public class PackageVoService {

	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private ApplyService applyService;
	
	/**
	 * 组装TaskVoList
	 * @param taskList
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:58:08
	 */
	public List<TaskVo> packageTaskVoList(List<Task> taskList) {
		if (CollectionUtils.isEmpty(taskList)) {
			return Collections.EMPTY_LIST;
		}
		
		List<TaskVo> taskVoList = Lists.newArrayList();
		for (Task task : taskList) {
			taskVoList.add(packageTaskVo(task));
		}
		return taskVoList;
	}

	/**
	 * 组装
	 * @param taskVoList
	 * @param task
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:59:10
	 */
	public TaskVo packageTaskVo(Task task) {
		if (task == null) {
			return null;
		}
		
		TaskVo taskVo = new TaskVo();
		BeanUtils.copyProperties(task, taskVo);
//		taskVo//
//			.setId(task.getId())//
//			.setName(task.getName())//
//			.setAssignee(task.getAssignee())//
//			.setFormKey(task.getFormKey())//
//			.setDescription(task.getDescription())//
//			.setCreateTime(task.getCreateTime())//
//			.setProcessDefinitionId(task.getProcessDefinitionId())//
//			.setProcessInstanceId(task.getProcessInstanceId());
		
		// 获取流程定义对象
		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()//
			.processDefinitionId(task.getProcessDefinitionId())//
			.singleResult();
		// 组装ProcessDefinitionVo对象
		ProcessDefinitionVo processDefinitionVo = this.packageProcessDefinitionVo(processDefinition);
		taskVo.setProcessDefinitionVo(processDefinitionVo);
		
		// 获取流程实例对象
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()//
			.processInstanceId(task.getProcessInstanceId())//
			.singleResult();
		// 组装ProcessInstanceVo对象
		ProcessInstanceVo processInstanceVo = this.packageProcessInstanceVo(processInstance);
		taskVo.setProcessInstanceVo(processInstanceVo);
		
		// 获取BusinessKey，即申请ID
		String applyId = processInstance.getBusinessKey();
		R<Apply> r = applyService.findById(new Long(applyId));
		if (!r.isSuccess()){
			throw new RuntimeException(r.getMsg());
		}
		taskVo.setLeave(r.getData());
		
		return taskVo;
	}
	
	/**
	 * 组装ProcessInstanceVoList
	 * @param processInstanceList
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:57:37
	 */
	public List<ProcessInstanceVo> packageProcessInstanceVoList(
			List<ProcessInstance> processInstanceList) {
		if (CollectionUtils.isEmpty(processInstanceList)) {
			return Collections.EMPTY_LIST;
		}
		
		List<ProcessInstanceVo> processInstanceVoList = Lists.newArrayList();
		for (ProcessInstance pi : processInstanceList) {
			processInstanceVoList.add(packageProcessInstanceVo(pi));
		}
		
		return processInstanceVoList;
	}

	/**
	 * 组装ProcessInstanceVo
	 * @param pi
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:57:47
	 */
	public ProcessInstanceVo packageProcessInstanceVo(ProcessInstance pi) {
		if (pi == null) {
			return null;
		}
		ProcessInstanceVo vo = new ProcessInstanceVo();
		BeanUtils.copyProperties(pi, vo);
		return vo;
	}
	
	/**
	 * 组装ModelVo对象集合
	 * @param modelList 模型集合
	 * @return List<ModelVo>
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:43:25
	 */
	public List<ModelVo> packageModelVoList(List<Model> modelList) {
		if (CollectionUtils.isEmpty(modelList)) {
			return Collections.EMPTY_LIST;
		}
		
		List<ModelVo> modelVoList = Lists.newArrayList();
		for (Model model : modelList) {
			ModelVo modelVo = new ModelVo();
			BeanUtils.copyProperties(model, modelVo);
			modelVoList.add(modelVo);
		}
		return modelVoList;
	}

	/**
	 * 组装ProcessDefinitionVoList
	 * @param processDefinitionList
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:44:53
	 */
	public List<ProcessDefinitionVo> packageProcessDefinitionVoList(
			Collection<ProcessDefinition> processDefinitionList) {
		if (CollectionUtils.isEmpty(processDefinitionList)) {
			return Collections.EMPTY_LIST;
		}
		
		List<ProcessDefinitionVo> result = Lists.newArrayList();
		for (ProcessDefinition pd : processDefinitionList) {
			result.add(packageProcessDefinitionVo(pd));
		}
		return result;
	}

	/**
	 * 组装ProcessDefinitionVo
	 * @param pd
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:45:10
	 */
	public ProcessDefinitionVo packageProcessDefinitionVo(ProcessDefinition pd) {
		if (pd == null) {
			return null;
		}
		
		ProcessDefinitionVo vo = new ProcessDefinitionVo();
		BeanUtils.copyProperties(pd, vo);
		return vo;
	}
	
	/**
	 * 组装HistoricProcessInstanceVoList
	 * @param historicProcessList
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:51:02
	 */
	public List<HistoricProcessInstanceVo> packageHistoricProcessInstanceVoList(
			List<HistoricProcessInstance> historicProcessList) {
		List<HistoricProcessInstanceVo> hpiVo = Lists.newArrayList();
		for (HistoricProcessInstance hpi : historicProcessList) {
			hpiVo.add(packageHistoricProcessInstanceVo(hpi));
		}	
		return hpiVo;
	}

	/**
	 * 组装HistoricProcessInstanceVo
	 * @param hpi
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月28日上午9:51:12
	 */
	public HistoricProcessInstanceVo packageHistoricProcessInstanceVo(
			HistoricProcessInstance hpi) {
		if (hpi == null) {
			return null;
		}
		
		HistoricProcessInstanceVo hpiVo = new HistoricProcessInstanceVo();
		BeanUtils.copyProperties(hpi, hpiVo);
		
		R<Apply> r = applyService.findById(new Long(hpi.getBusinessKey()));
//		if (r.isSuccess()){
//			hpiVo.setLeave(r.getData());
//		}
		return hpiVo;
	}
	
}
