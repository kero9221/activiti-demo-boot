package com.kero.service.activiti;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kero.common.Const;
import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.vo.ProcessDefinitionVo;

import lombok.extern.slf4j.Slf4j;

/**
 * 处理工作流与流程定义相关的service
 * @author 郑志良
 * @date 2018年8月15日上午11:00:35
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Slf4j
@Service
public class WorkflowProcessDefinitionService extends WorkflowBaseService {

	/**
	 * 查询最新版本的流程列表，并分页返回
	 * @param pageSize 分页参数，开始索引
	 * @param pageNum 分页参数，结束索引
	 * @param name 流程定义名称，如果传递了该参数，则按该参数进行模糊查询
	 * @return
	 */
	public R<PageResult<ProcessDefinition>> processList(int pageNum, int pageSize, String name) throws Exception {
		PageResult<ProcessDefinitionVo> page = new PageResult<>(pageNum, pageSize);
		ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
		
		// 如果传入了name参数，则按name进行模型查询
		if (StringUtils.isNotBlank(name)){
			query.processDefinitionNameLike("%" + name + "%");
		}
		
		// 查询流程定义，并按照ID进行升序排序，该排序用来过滤出最新版本的流程定义
		List<ProcessDefinition> processDefinitionList = query.orderByProcessDefinitionId().asc().active().list();
		// 如果结果集为空，则返回一个空集合
		if (CollectionUtils.isEmpty(processDefinitionList)){
			page.setTotal(0);
			page.setRows(Collections.EMPTY_LIST);
			return R.success("没有相关的记录~").data(page);
		}
		
		// 使用map过滤出最新版本的流程定义
		Map<String, ProcessDefinition> filterLastVersionMap = new LinkedHashMap<String, ProcessDefinition>();
		for (ProcessDefinition pd : processDefinitionList) {
			// 只保留最新版本的流程定义，因为多个版本的key相同，新的value会覆盖旧的value
			filterLastVersionMap.put(pd.getKey(), pd);
		}
		
		// 这里使用Java8的stream特性对返回 的结果集进行排序和分页
//		List<ProcessDefinition> lastVersionList = filterMap.values().stream()//
//				.sorted((x, y) -> {
//					return x.getKey().compareTo(y.getKey());
//				})// 设置排序规则
//				.skip(page.getFirstIndex())//跳过的记录数，类似MySQL中分页参数的开始索引
//				.limit(page.getPageSize())//取多少条记录
//				.collect(Collectors.toList());// 将过滤后的数据封装到一个list中
		
		// 组装ProcessDefinitionVo对象
		List<ProcessDefinitionVo> result = packageVoService.packageProcessDefinitionVoList(filterLastVersionMap.values());
		
		page.setRows(result);
		page.setTotal(result.size());
		
		return R.success().data(page);
	}
	
	/**
	 * 根据流程定义ID查询流程定义
	 * @param processDefinitionId 流程定义
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午11:31:58
	 */
	public R<ProcessDefinition> findProcessById(String processDefinitionId) throws Exception {
		if (StringUtils.isBlank(processDefinitionId)){
			return R.error("参数错误， processDefnitionId不能为空");
		}
		
		ProcessDefinition processDefinition = repositoryService//
												.createProcessDefinitionQuery()//
												.processDefinitionId(processDefinitionId)//
												.singleResult();
		if (processDefinition == null){
			return R.error("没有找到对应的流程定义，processDefinitionId: " + processDefinitionId);
		}
		return R.success().data(processDefinition);
	}
	
	/**
	 * 删除流程定义，级联删除流程实例
	 * @param deploymentId 流程部署ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午11:32:22
	 */
	public R delete(String deploymentId) throws Exception {
		if (StringUtils.isBlank(deploymentId)){
			return R.error("参数错误，deploymentId不能为空");
		}
		
		try {
			repositoryService.deleteDeployment(deploymentId, true);
			return R.success("删除流程定义成功");
		} catch (Exception e) {
			log.error("删除流程定义失败, deploymentId: {}", deploymentId, e);
			return R.error("删除流程定义失败");
		}
	}
	
	/**
	 * 将流程定义转换成模型
	 * @param processDefinitionId 流程定义ID
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月15日上午11:58:58
	 */
	public R convertToModel(String processDefinitionId) throws Exception{
		// 查询出流程定义对象
		R<ProcessDefinition> r = findProcessById(processDefinitionId);
		if (!r.isSuccess()){
			return r;
		}
		ProcessDefinition processDefinition = r.getData();
		
		// 获取流程定义对应的资源输入流
		String deploymentId = processDefinition.getDeploymentId();
		String resourceName = processDefinition.getResourceName();
		InputStream inputStream = repositoryService.getResourceAsStream(deploymentId, resourceName);
		
		// 将输入流转换成BpmnModel
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			InputStreamReader reader = new InputStreamReader(inputStream, "UTF-8");
			XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(reader);
			// 转换成BpmnModel
			BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xmlStreamReader);
			// 将BpmnModel转换成ObjectNode
			ObjectNode modelResourceNode = new BpmnJsonConverter().convertToJson(bpmnModel);
			
			// 创建并设置model相关属性，如：key,name,category...
			Model newModel = repositoryService.newModel();
			newModel.setKey(processDefinition.getKey());
			newModel.setName(processDefinition.getName());
			newModel.setCategory(processDefinition.getDeploymentId());
			
			// 设置model的MetaInfo属性
			ObjectNode modelEditorNode = objectMapper.createObjectNode();
			modelEditorNode.put(ModelDataJsonConstants.MODEL_NAME, processDefinition.getName());
			modelEditorNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
			modelEditorNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, processDefinition.getDescription());
			newModel.setMetaInfo(modelEditorNode.toString());
			
			// 保存model信息
			repositoryService.saveModel(newModel);
			// 保存ModelEditorSource信息
			repositoryService.addModelEditorSource(newModel.getId(), modelResourceNode.toString().getBytes("UTF-8"));
			log.info("转换成模型成功，modelId: {}", newModel.getId());
			
			return R.success("转换成Model成功！modelId: " + newModel.getId());
		} catch (Exception e) {
			log.error("转换成Model失败", e);
			throw new RuntimeException("流程转换成Model失败, processDefinitionId: " + processDefinitionId, e);
		} 
	}
	
	/**
	 * 加载指定名称的流程资源（.bpmn.xml文件或流程图）
	 * @param deploymentId 流程部署ID
	 * @param resourceType 流程资源名称
	 * @return
	 * @throws Exception
	 */
	public R<InputStream> loadResource(String processDefinitionId, String resourceType) throws Exception {
		if (StringUtils.isBlank(processDefinitionId) || StringUtils.isBlank(resourceType)){
			log.error("参数错误， deploymentId 和 resourceName 都不能为空");
			return R.error("参数错误， deploymentId 和 resourceName 都不能为空");
		}
		
		ProcessDefinition processDefinition = repositoryService
												.createProcessDefinitionQuery()
												.processDefinitionId(processDefinitionId)
												.singleResult();
		if (processDefinition == null){
			log.error("没有找到对应流程定义对象, processDefinitionId: {}", processDefinitionId);
			return R.error("没有找到对应流程定义对象, processDefinitionId: " + processDefinitionId);
		}
		
		String resourceName = "";
		if (Const.ProcessDefinitionResourceType.XML.equals(resourceType)){
			resourceName = processDefinition.getResourceName();
		} else if (Const.ProcessDefinitionResourceType.IMAGE.equals(resourceType)){
			resourceName = processDefinition.getDiagramResourceName();
		}
		
		try {
			if (StringUtils.isBlank(resourceName)){
				return R.error("没有找到对应的资源");
			}
			
			InputStream inputStream = repositoryService.getResourceAsStream(processDefinition.getDeploymentId(), resourceName);
			if (inputStream == null){
				return R.error("没找到该流程对应的" + resourceType + "资源");
			}
			return R.success().data(inputStream);
		} catch (Exception e) {
			log.error("没有找到该流程对应的资源，processDefinitionId： " + processDefinition.getId());
			throw new RuntimeException("没有找到该流程对应的资源，processDefinitionId: " + processDefinition.getId() + ", 资源文件：" + resourceType, e);
		}
	}
	
	/**
	 * 部署流程定义
	 * @param filename 流程定义的名称
	 * @param inputStream 部署资源的输入流
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月8日上午8:48:38
	 */
	public R deploy(MultipartFile file) throws Exception {
		if (file == null || file.isEmpty()){
			log.error("===部署的资源为空，部署失败");
			return R.error("部署失败，部署资源为空");
		}
		
		String filename = file.getOriginalFilename();
		InputStream inputStream = file.getInputStream();
		// 将文件输入流转换成zip输入流
		ZipInputStream zipInputStream = new ZipInputStream(inputStream);
		// 部署流程定义
		Deployment deployment = repositoryService.createDeployment()//
									.name(filename)//
									.addZipInputStream(zipInputStream)//
									.deploy();
		
		log.info("流程部署成功，部署ID: {}", deployment.getId());
		return R.success("流程部署成功，部署ID: " + deployment.getId());
	}

	
}
