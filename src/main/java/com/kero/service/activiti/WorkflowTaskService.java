package com.kero.service.activiti;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.vo.TaskVo;

/**
 * 处理工作流中与任务相关的Service
 * 
 * @author 郑志良
 * @date 2018年8月16日上午10:53:18
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Service
public class WorkflowTaskService extends WorkflowBaseService {

	/**
	 * 任务签收
	 * @param taskId 任务ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月9日下午4:53:17
	 */
	public R claim(String taskId, String userId) throws Exception {
		if (StringUtils.isBlank(taskId)){
			return R.PARAM_ERROR;
		}
		
		// 创建任务查询对象
		TaskQuery taskQuery = taskService.createTaskQuery().taskId(taskId);
		// 校验任务是否存在
		long count = taskQuery.count();
		if (count != 1){
			return R.error("签收的任务不存在，taskId: " + taskId);
		}
		
		// 校验当前登录的用户是否有权限办理taskId对应的任务
		count = taskQuery.taskCandidateOrAssigned(userId).count(); 
		if (count != 1){
			return R.error("没有权限签收该任务");
		}
		
		// 校验任务是否已经被签收
		count = taskQuery.taskAssignee(userId).count();
		if (count == 1) {
			return R.error("该任务已经被签收，可直接办理，taskId: " + taskId);
		}
		
		// 签收任务
		taskService.claim(taskId, userId);
		
		return R.success("任务签收成功！taskId: " + taskId + ", 签收人：" + userId);
	}
	
	/**
	 * 查询当前登录用户的待办任务列表
	 * @param userId 任务办理人
	 * @return
	 *
	 * @author 郑志良
	 * @param endTime 
	 * @param startTime 
	 * @param pageSize 
	 * @param pageNum 
	 * @date 2018年8月7日上午11:16:04
	 */
	public R<PageResult<TaskVo>> findTodoTaskList(String userId, Date startTime, Date endTime, int pageNum, int pageSize) throws Exception {
		if (StringUtils.isBlank(userId)){
			return R.NEED_LOGIN;
		}
		
		PageResult<TaskVo> page = new PageResult<TaskVo>(pageNum, pageSize);
		// 创建任务查询对象
		TaskQuery query = taskService.createTaskQuery();
		// 拼接查询条件
		if (startTime != null){
			query.taskCreatedAfter(startTime);
		}
		if (endTime != null){
			query.taskCreatedBefore(endTime);
		}
		
	    // 查询待办任务，包括已经签收和未签收的任务
		List<Task> taskList = query
								.taskCandidateOrAssigned(userId)// 
								.active()// 只查找激活状态的任务
								.orderByTaskCreateTime().desc()// 根据任务的创建时间降序排序
								.listPage(page.getFirstIndex(), page.getPageSize());
		
		// 如果待办任务为空，则返回一个空的集合
		if (CollectionUtils.isEmpty(taskList)){
			return R.success("没有找到相关的记录~").data(Collections.EMPTY_LIST);
		}
		
		List<TaskVo> taskVoList = packageVoService.packageTaskVoList(taskList);
		page.setTotal(query.count());
		page.setRows(taskVoList);
		
		return R.success().data(page);
	}

}
