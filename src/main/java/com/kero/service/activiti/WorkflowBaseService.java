/**
 * 
 */
package com.kero.service.activiti;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 该类注入了工作流使用到的接口，方便子类直接调用而无需重复注入
 * @author 郑志良
 * @date 2018年8月8日上午10:17:30
 */
public class WorkflowBaseService {

	@Autowired
	protected ProcessEngine processEngine;
	@Autowired
	protected RepositoryService repositoryService;
	@Autowired
	protected FormService formService;
	@Autowired
	protected RuntimeService runtimeService;
	@Autowired
	protected IdentityService identityService;
	@Autowired
	protected TaskService taskService;
	@Autowired
	protected HistoryService historyService;
	@Autowired
	protected ManagementService managementService;
	
	@Autowired
	protected PackageVoService packageVoService;
	
	@Autowired
	protected ObjectMapper objectMapper;
	
}
