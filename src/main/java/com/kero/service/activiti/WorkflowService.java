package com.kero.service.activiti;

import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.form.StartFormData;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.SuspensionState;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.vo.HistoricProcessInstanceVo;
import com.kero.vo.ProcessInstanceVo;
import com.kero.vo.TaskVo;

import lombok.extern.slf4j.Slf4j;

/**
 * 流程相关操作的业务层实现
 * @author kero
 * @date 2018年8月6日下午10:17:49
 */
@SuppressWarnings({"rawtypes"})
@Slf4j
@Service
public class WorkflowService extends WorkflowBaseService {

	@Autowired
	private WorkflowProcessDefinitionService processDefinitionService;
	@Autowired
	private WorkflowProcessInstanceService processInstanceService;
	@Autowired
	private WorkflowTaskService workflowTaskService;
	
	//---------------------------- processDefnition start ------------------------------
	/**
	 * 将流程定义转换成模型
	 * @param processDefinitionId 流程定义ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日下午12:08:41
	 */
	public R convertToModel(String processDefinitionId) throws Exception{
		return processDefinitionService.convertToModel(processDefinitionId);
	}
	
	/**
	 * 删除流程定义，级联删除流程实例
	 * @param deploymentId
	 * @return
	 * @throws Exception 
	 */
	public R deleteProcess(String deploymentId) throws Exception{
		return processDefinitionService.delete(deploymentId);
	}
	
	/**
	 * 查询最新版本的流程定义，带分页返回
	 * @param pageSize 分页参数，开始索引
	 * @param pageNum 分页参数，每页显示的记录灵敏
	 * @param name 流程定义名称
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月15日上午11:53:03
	 */
	public R<PageResult<ProcessDefinition>> processList(int pageNum, int pageSize,
			String name) throws Exception {
		return processDefinitionService.processList(pageNum, pageSize, name);
	}
	
	/**
	 * 加载指定名称的流程资源（.bpmn.xml文件或流程图）
	 * @param deploymentId 流程部署ID
	 * @param resourceType 流程资源名称，包括ProcessDefinitionResourceType.XML和ProcessDefinitionResourceType.IMAGE
	 * @return
	 * @throws Exception
	 */
	public R<InputStream> loadResource(String processDefinitionId, String resourceType) throws Exception {
		return processDefinitionService.loadResource(processDefinitionId, resourceType);
	}
	
	/**
	 * 部署流程定义
	 * @param filename 流程定义的名称
	 * @param inputStream 部署资源的输入流
	 *
	 * @author 郑志良
	 * @return 
	 * @throws Exception 
	 * @date 2018年8月8日上午8:48:38
	 */
	public R deploy(MultipartFile file) throws Exception {
		return processDefinitionService.deploy(file);
	}
	
	//---------------------------- processDefnition end ------------------------------
	
	
	
	//---------------------------- processInstance start ------------------------------
	
	/**
	 * 挂起或激活流程实例
	 * @param state 状态，该状态包括suspend(挂起)和active(激活)
	 * @param processInstanceId 流程实例ID
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月7日上午10:34:58
	 */
	public R suspendedOrActive(String state, String processInstanceId) throws Exception {
		return processInstanceService.suspendedOrActive(state, processInstanceId);
	}	
	
	/**
	 * 查询运行状态的流程实例
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月7日上午9:28:33
	 */
	public R<List<ProcessInstanceVo>> findRunningProcessList() throws Exception{
		return processInstanceService.findProcessInstanceByStatus(SuspensionState.ACTIVE.getStateCode());
	}
	
	/**
	 * 查询挂起状态的流程实例
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月7日上午9:28:33
	 */
	public R<List<ProcessInstanceVo>> findSuspendedProcessList() throws Exception{
		return processInstanceService.findProcessInstanceByStatus(SuspensionState.SUSPENDED.getStateCode());
	}
	
	/**
	 * 删除正在运行的流程实例
	 * @param processInstanceId 流程实例ID
	 * @param deleteReason 删除原因
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月13日上午10:26:47
	 */
	public R deleteRunningProcessInstance(String processInstanceId, String deleteReason) throws Exception{
		return processInstanceService.deleteRunningProcessInstance(processInstanceId, deleteReason);
	}
	
	/**
	 * 删除历史流程实例
	 * @param processInstanceId 流程实例ID
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月13日上午10:28:32
	 */
	public R deleteFinishedProcessInstance(String processInstanceId) throws Exception{
		return processInstanceService.deleteFinishedProcessInstance(processInstanceId);
	}
	
	/**
	 * 查询已经完成的流程实例
	 * @param userId 用户ID
	 * @return 
	 *
	 * @author 郑志良
	 * @param pageSize
	 * @param pageNum 
	 * @throws Exception 
	 * @date 2018年8月7日下午3:33:20
	 */
	public R<PageResult<HistoricProcessInstanceVo>> findFinishedProcessInstance(String userId, int pageNum, int pageSize) throws Exception {
		return processInstanceService.findFinishedProcessInstance(userId, pageNum, pageSize);
	}
	
	//---------------------------- processInstance end ------------------------------
	
	/**
	 * 查询所有最新版本的流程定义
	 * @return 
	 * @throws Exception
	 */
	public Collection<ProcessDefinition> processList() throws Exception {
		// 查看所有版本的流程定义
		List<ProcessDefinition> allList = repositoryService
											.createProcessDefinitionQuery()//
											.orderByDeploymentId().asc()// 按部署ID进行升序排序
											.list();
		
		if (CollectionUtils.isEmpty(allList)){
			return Collections.emptyList();
		}
		
		// 使用map过滤出最新版本的流程定义
		Map<String, ProcessDefinition> filterMap = new LinkedHashMap<String, ProcessDefinition>();
		for (ProcessDefinition pd : allList) {
			// 只保留最新版本的流程定义
			filterMap.put(pd.getKey(), pd);
		}
		
		// 对流程定义的资源名称及图片名称进行处理，当长度大于16时，后面使用"..."代替
		Collection<ProcessDefinition> values = filterMap.values();
		for (ProcessDefinition pd : values) {
			ProcessDefinitionEntity pde = (ProcessDefinitionEntity) pd;
			String resourceName = pde.getResourceName();
			if (StringUtils.isNotBlank(resourceName)){
				if (resourceName.length() > 16){
					pde.setResourceName(resourceName.substring(0, 16) + "...");
				}
			}
			
			String diagramResourceName = pde.getDiagramResourceName();
			if (StringUtils.isNotBlank(diagramResourceName)){
				if (diagramResourceName.length() > 16){
					pde.setDiagramResourceName(diagramResourceName.substring(0, 16) + "...");
				}
			}
		}
		
		return filterMap.values();
	}
	
	/**
	 * 获取流程开始表单属性
	 * @param processDefinitionId 流程定义ID
	 * @return
	 */
	public StartFormData getStartFormData(String processDefinitionId){
		if (StringUtils.isBlank(processDefinitionId)){
			return null;
		}
		
		StartFormData startFormData = formService.getStartFormData(processDefinitionId);
		return startFormData;
	}
	
	/**
	 * 启动流程实例，并设置启动表单属性
	 * @param processDefinitionId 流程定义的ID
	 * @param properties 启动表单属性
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月7日上午10:17:44
	 */
	public ProcessInstance startProcessInstance(String processDefinitionId,
			Map<String, String> properties) {
		ProcessInstance processInstance = formService.submitStartFormData(processDefinitionId, properties);
		return processInstance;
	}


	/**
	 * 查询当前登录用户的待办任务列表
	 * @param assignee 任务办理人
	 * @return
	 *
	 * @author 郑志良
	 * @param endTime 
	 * @param startTime 
	 * @param pageSize 
	 * @param pageNum 
	 * @throws Exception 
	 * @date 2018年8月7日上午11:16:04
	 */
	public R<PageResult<TaskVo>> findTodoTaskList(String userId, Date startTime, Date endTime, int pageNum, int pageSize) throws Exception {
		return workflowTaskService.findTodoTaskList(userId, startTime, endTime, pageNum, pageSize);
	}

	/**
	 * 签收任务
	 * @param taskId 任务ID
	 * @param userId 用户ID
	 * @return
	 *
	 * @author 郑志良
	 * @throws Exception 
	 * @date 2018年8月7日上午11:45:42
	 */
	public R claim(String taskId, String userId) throws Exception {
		return workflowTaskService.claim(taskId, userId);
	}

	
	/**
	 * 查询当前任务的坐标信息
	 * @param processDefinitionId 流程实例ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月7日下午4:49:47
	 */
	@SuppressWarnings("unchecked")
	public R<Map<String, Object>> findCurrentTaskCoordinate(String processInstanceId){
		if (StringUtils.isBlank(processInstanceId)) {
			return R.error("参数错误，processInstanceId不能为空");
		}
		
		// 查询出所有该流程实例所有执行实例
		List<Execution> executionList = runtimeService.createExecutionQuery()//
											.processInstanceId(processInstanceId)//
											.list();

		String activityId = null;
		String processDefinitionId = null;
		for (Execution execution : executionList) {
			String tempId = execution.getActivityId();
			if (StringUtils.isNotBlank(tempId)){
				activityId = tempId;
				processDefinitionId = ((ExecutionEntity)execution).getProcessDefinitionId();
				break;
			}
		}
		
		if (activityId == null){
			throw new RuntimeException("没有找到当前的任务，processInstanceId: " + processInstanceId);
		}
		
		log.info("###activityId: {}, processDefinitionId: {}", activityId, processDefinitionId);
		
		// 获取流程定义实体对象
		ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) repositoryService.getProcessDefinition(processDefinitionId);
		// 通过流程定义实例对象，找到当前活动的任务信息对象
		ActivityImpl activityImpl = processDefinitionEntity.findActivity(activityId);
		
		// 获取当前任务和节点的坐标
		Map<String, Object> coordinates = new HashMap<String, Object>();
		coordinates.put("x", activityImpl.getX());
		coordinates.put("y", activityImpl.getY());
		coordinates.put("width", activityImpl.getWidth());
		coordinates.put("height", activityImpl.getHeight());
		
		// 封装用于返回的数据
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("processDefinitionId", processDefinitionId);
		resultMap.put("acs", coordinates);
		
		return R.success().data(resultMap);
	}

	/**
	 * 根据任务ID查询任务对象
	 * @param taskId 任务ID
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月10日上午11:35:43
	 */
	public Task findTaskByTaskId(String taskId) {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		return task;
	}
	
}
