package com.kero.service.activiti;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ModelQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kero.common.PageResult;
import com.kero.common.R;
import com.kero.vo.ModelVo;

import lombok.extern.slf4j.Slf4j;

/**
 * 处理与model相关业务
 * @author 郑志良
 * @date 2018年8月13日下午3:54:43
 */
@SuppressWarnings({"unchecked", "rawtypes"})
@Slf4j
@Service
public class WorkflowModelService extends WorkflowBaseService {

	/**
	 * 部署流程定义
	 * @param modelId 模型ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月13日下午5:42:54
	 */
	public R deploy(String modelId) throws Exception {
		if (StringUtils.isBlank(modelId)){
			return R.error("参数错误，modelId不能为空");
		}
		
		// 根据modelId获取model
		Model model = repositoryService.getModel(modelId);
		try {
			// 读取模型的xml信息
			JsonNode modelJsonNode = objectMapper.readTree(repositoryService.getModelEditorSource(modelId));
			
			// 将模型转换成BpmnMode，再将BpmnModel转换成字节数组
			BpmnModel bpmnModel = new BpmnJsonConverter().convertToBpmnModel(modelJsonNode);
			byte[] modelBytes = new BpmnXMLConverter().convertToXML(bpmnModel);
			
			// 设置xml文件的名称
			String resourceName = model.getName() + ".bpmn20.xml"; 
			// 通过addString的方式部署流程定义
			Deployment deployment = repositoryService.createDeployment()//
										.name(model.getName())//
										.addString(resourceName, new String(modelBytes))//
										.deploy();
			
			log.info("流程部署成功，部署ID: {}", deployment.getId());
			
			return R.success("流程部署成功，部署ID: " + deployment.getId());
		} catch (Exception e) {
			log.error("流程部署失败，模型ID: {}", model.getId(), e);
			throw new RuntimeException("流程部署失败，模型ID: " + model.getId());
		}
	}
	
	/**
	 * 创建模型
	 * @param name 模型名称 
	 * @param key 模型key
	 * @param description 模型描述
	 * @return 返回模型ID，前端页面需要使用到该模型ID来跳转到模型编辑页面
	 *
	 * @author 郑志良
	 * @date 2018年8月13日下午5:25:02
	 */
	@Transactional
	public R<String> create(ModelVo modelVo) throws Exception {
		try {
			// 处理入参
			String name = StringUtils.defaultString(modelVo.getName());
			String key = StringUtils.defaultString(modelVo.getKey());
			String description = StringUtils.defaultString(modelVo.getDescription());
			
			log.info("name: {}, key: {}, description: {}", name, key, description);
			
			// 组装模型相关信息，如name,reversion,description
			ObjectNode modelNode = objectMapper.createObjectNode();
			modelNode.put(ModelDataJsonConstants.MODEL_NAME, name);
			modelNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
			modelNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, description);
			
			// 组装.bpmn.xml文件相关信息
			ObjectNode stencilSetNode = objectMapper.createObjectNode();
			stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
			ObjectNode editorNode = objectMapper.createObjectNode();
			editorNode.put("stencilset", stencilSetNode);
//			editorNode.set("stencilset", stencilSetNode);
			editorNode.put("id", "canvas");
			editorNode.put("resourceId", "canvas");
			
			// 创建模型对象，并设置相关属性
			Model model = repositoryService.newModel();
			model.setName(name);
			model.setKey(key);
			model.setMetaInfo(modelNode.toString());
			
			// 保存模型和模型的xml信息
			repositoryService.saveModel(model);
			repositoryService.addModelEditorSource(model.getId(), editorNode.toString().getBytes("UTF-8"));
			
			// 返回已经保存的模型的ID，前端页面需要使用到该modelID来跳转到模型编辑页面
			return R.success().data(model.getId());
		} catch (UnsupportedEncodingException e) {
			log.error("创建模型失败，name:{}, key:{}, description:{}", modelVo.getName(), modelVo.getKey(), modelVo.getDescription(), e);
			throw new RuntimeException("创建模型失败");
		}
	}
	
	/**
	 * 删除模型
	 * @param modelId 模型ID
	 * @return
	 * @throws Exception
	 *
	 * @author 郑志良
	 * @date 2018年8月13日下午5:18:07
	 */
	@Transactional
	public R delete(String modelId) throws Exception {
		if (StringUtils.isBlank(modelId)){
			return R.error("参数错误，modelId不能为空");
		}
		
		try {
			repositoryService.deleteModel(modelId);
			return R.success("模型删除成功");
		} catch (Exception e) {
			log.error("模型删除失败，modelId: {}", modelId, e);
			throw new RuntimeException("模型删除失败， modelId: " + modelId);
		}
	}
	
	/**
	 * 查询模型列表
	 * @param pageSize 分页参数，开始位置
	 * @param pageNum 分页参数，获取多少条数据
	 * @param modelName 模型名称
	 * @return
	 *
	 * @author 郑志良
	 * @date 2018年8月13日下午4:00:44
	 */
	public R<PageResult<ModelVo>> list(int pageSize, int pageNum, String modelName) throws Exception {
		ModelQuery query = repositoryService.createModelQuery();
		
		// 如果传递了modelName属性，则按该属性模糊查询
		if (StringUtils.isNotBlank(modelName)){
			query.modelNameLike("%" + modelName + "%");
		}
		
		PageResult<ModelVo> page = new PageResult<ModelVo>(pageNum, pageSize);
		List<Model> modelList = query.orderByCreateTime().desc().listPage(page.getFirstIndex(), page.getPageSize());
		List<ModelVo> modelVoList = packageVoService.packageModelVoList(modelList);
		
		page.setTotal(query.count());
		page.setRows(modelVoList);
		
		return R.success().data(page);
	}

}
