package com.kero.vo;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain=true)
public class ModelVo {

	private String id;
	private String name;
	private String key;
	private Date createTime;
	private Date lastUpdateTime;
	private Integer version;
	private String deploymentId;
	private String metaInfo;
	private String description;
    
	// protected int revision;
	// protected String category;
	// protected String editorSourceValueId;
	// protected String editorSourceExtraValueId;
}
