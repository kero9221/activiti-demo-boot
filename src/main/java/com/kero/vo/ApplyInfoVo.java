package com.kero.vo;

import com.kero.entity.ApplyInfo;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain=true)
public class ApplyInfoVo extends ApplyInfo {

	private HistoricProcessInstanceVo historicProcessInstanceVo;
	
}
