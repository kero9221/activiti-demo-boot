package com.kero.vo;

import java.util.Date;

import com.kero.entity.Apply;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


@Getter
@Setter
@NoArgsConstructor
@Accessors(chain=true)
public class TaskVo {

	private String id;
	private String name;
	private String processInstanceId;
	private String assignee;
	private Date createTime;
	private String processDefinitionId;
	
	//新增
	private String applyName;
	private Date applyTime;
	private String description;
	private String formKey;
	
	private ProcessDefinitionVo processDefinitionVo;
	private ProcessInstanceVo processInstanceVo;
	private Apply leave;
	
}
