package com.kero.vo;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain=true)
public class ProcessInstanceVo {

	private String id;
	private String deploymentId;
	private String name;
	private Date startTime;
	private Date endTime;
	private Long durationInMillis; 
	private String businessKey;
	private String description;
	private String processDefinitionId;
	private String processDefinitionName;
	private String processDefinitionKey;
	private int processDefinitionVersion;
	private String startActivityId;
	
}
