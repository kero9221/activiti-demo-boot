package com.kero.vo;

import com.kero.entity.Apply;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ApplyVo extends Apply {

	private HistoricProcessInstanceVo historicProcessInstanceVo;
	
}
