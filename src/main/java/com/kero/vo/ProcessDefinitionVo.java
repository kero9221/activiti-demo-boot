package com.kero.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain=true)
public class ProcessDefinitionVo {
	
	private String id;
	private String deploymentId;
	private String key;
	private String resourceName;
	private int version;
	private String name;
	private String diagramResourceName;
	private String description;
}
