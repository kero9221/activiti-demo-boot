package com.kero.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 用来接收页面参数的实例
 * 
 * @author 郑志良
 * @date 2018年8月10日上午9:05:53
 */
@Setter
@Getter
@NoArgsConstructor
@Accessors(chain=true)
public class WorkflowBean {

	/** 是否同意，true：同意，false：不同意 */
	private boolean approve;
	/** 审批意见 */
	private String comment;
	/** 任务ID */
	private String taskId;
	/** 请假单ID */
	private Long leaveId;
	/** 请假表单信息的JSON串 */
	private String formData;
	/** 任务办理候选人 */
	private String candidateUsers;
	/** 请假单ID */
	private Long id;
	
}
