package com.kero.vo;

import java.util.Date;

import com.kero.entity.Apply;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@NoArgsConstructor
@Accessors(chain=true)
public class HistoricProcessInstanceVo {

	private String id;
	private String name;
	private String description;
	private String businessKey;
	private String processInstanceId;
	private String deploymentId;
	private Date startTime;
	private Date endTime;
	private Long durationInMillis;
	private String deleteReason;
	private String startUserId;
	private String endActivityId;
	private String startActivityId;
	
	private String processDefinitionKey;
	private String processDefinitionName;
	private String processDefinitionId;
	private Integer processDefinitionVersion;
	private String superProcessInstanceId;
	
//	private Leave leave;
	
}
