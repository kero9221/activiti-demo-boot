package com.kero.exception;

/**
 * 数据库异常
 * @author 郑志良
 * @date 2018年8月14日上午11:59:35
 */
public class DBException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	public DBException() {
		super();
	}
	
	public DBException(String message) {
		super(message);
		this.message = message;
	}
	
	public DBException(String message, Throwable e) {
		super(message, e);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
//	public static final DBException DB_INSERT_ERROR = new DBException("数据库异常，数据插入失败~");
//	public static final DBException DB_UPDATE_ERROR = new DBException("数据库异常，数据更新失败~");
//	public static final DBException DB_DELETE_ERROR = new DBException("数据库异常，数据删除失败~");
//	public static final DBException DB_SELECT_ERROR = new DBException("数据库异常，数据查询失败~");
	
}
