package com.kero.util;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;

/**
 * 与User相关操作的工具类
 * @author kero
 * @date 2018年8月6日下午9:50:57
 */
public class UserUtil {

	private static final String SESSION_USER = "user";
	private static final String SESSION_GROUPS = "groups";
	private static final String SESSION_GROUP_NAMES = "groupNames";
	
	/**
	 * 从session中获取当前登录的用户信息
	 * @param session
	 * @return
	 */
	public static User getSessionUser(HttpSession session){
		Object obj = session.getAttribute(SESSION_USER);
		return obj == null ? null : (User)obj;
	}
	
	/**
	 * 将当前登录的用户保存到session中
	 * @param session
	 * @param user
	 */
	public static void saveUserToSession(HttpSession session, User user){
		session.setAttribute(SESSION_USER, user);
	}
	
	/**
	 * 将当前登录的用户信息从session中删除
	 * @param session
	 * @param user
	 */
	public static void removeSessionUser(HttpSession session, User user){
		session.removeAttribute(SESSION_USER);
	}
	
	/**
	 * 将当前登录用户的所有权限组信息保存到session中
	 * @param session
	 * @param groupList
	 */
	public static void saveGroupToSession(HttpSession session, List<Group> groupList){
		session.setAttribute(SESSION_GROUPS, groupList);
	}
	
	/**
	 * 将当前登录用户的所有权限组名称保存到session中，多个权限组之间使用","分隔
	 * @param session
	 * @param groupNames 所有权限组的名称
	 */
	public static void saveGroupNamesToSession(HttpSession session, String groupNames){
		session.setAttribute(SESSION_GROUP_NAMES, groupNames);
	}
	
	/**
	 * 退出登录时，清除当前用户信息和权限组信息
	 * @param session
	 */
	public static void logout(HttpSession session){
		session.removeAttribute(SESSION_USER);
		session.removeAttribute(SESSION_GROUPS);
		session.removeAttribute(SESSION_GROUP_NAMES);
	}
	
}
